import json
import requests
import os
import time
import operator
import re
from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc, exc, func, cast
from majors_and_fields_translations import (
    get_area_of_study_from_major,
    get_major_from_college_scorecard_area_of_study,
)


postgres_local = os.environ.get("POSTGRES")
# postgres_local = os.environ.get("POSTGRES_LOCAL")
key = os.environ.get("SEARCH_KEY")


application = Flask(__name__)
CORS(application, resources={r"/*": {"origins": "*"}})
application.config["DEBUG"] = True

if postgres_local is None:
    postgres_local = ""
application.config["SQLALCHEMY_DATABASE_URI"] = (
    "postgresql://swetea:"
    + postgres_local
    + "@aa1tvfjlqm84gfp.cjhuh74i2e6l.us-east-1.rds.amazonaws.com:5432/postgres"
)

# application.config["SQLALCHEMY_DATABASE_URI"] = (
#     "postgresql://postgres:"
#     + postgres_local
#     + "@localhost:5432/postgres"
# )


db = SQLAlchemy(application)


class Universities(db.Model):
    school_id = db.Column(db.Integer, nullable=False, primary_key=True)
    school_name = db.Column(db.String(), nullable=False)
    school_alias = db.Column(db.String())
    city = db.Column(db.String(), nullable=False)
    abbrv_state = db.Column(db.String(), nullable=False)
    state = db.Column(db.String(), nullable=False)
    school_url = db.Column(db.String())
    price_calculator_url = db.Column(db.String())
    latitude = db.Column(db.Float())
    longitude = db.Column(db.Float())
    undergrad_size = db.Column(db.Integer())
    race_ethnicity_white = db.Column(db.Float())
    race_ethnicity_black = db.Column(db.Float())
    race_ethnicity_hispanic = db.Column(db.Float())
    race_ethnicity_asian = db.Column(db.Float())
    race_ethnicity_aian = db.Column(db.Float())
    race_ethnicity_nhpi = db.Column(db.Float())
    race_ethnicity_two_or_more = db.Column(db.Float())
    race_ethnicity_non_resident_alien = db.Column(db.Float())
    race_ethnicity_unknown = db.Column(db.Float())
    graduation_rate = db.Column(db.Float())
    median_income = db.Column(db.Integer())
    percent_of_degrees = db.Column(db.JSON(), nullable=False)
    admission_rate = db.Column(db.Float())
    sat_average = db.Column(db.Float())
    act_midpoint = db.Column(db.Float())
    in_state_tuition = db.Column(db.Integer())
    out_of_state_tuition = db.Column(db.Integer())
    private_tuition = db.Column(db.Integer())
    public_tuition = db.Column(db.Integer())
    top_majors = db.Column(db.JSON(), nullable=False)
    top_majors_longer = db.Column(db.JSON(), nullable=False)
    images = db.Column(db.ARRAY(db.String()))


class Cities(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    state = db.Column(db.String())
    abbrv_state = db.Column(db.String())
    age = db.Column(db.Float)
    income = db.Column(db.Float)
    income_2ormore = db.Column(db.Float)
    income_asian = db.Column(db.Float)
    income_black = db.Column(db.Float)
    income_hawaiian = db.Column(db.Float)
    income_hispanic = db.Column(db.Float)
    income_native = db.Column(db.Float)
    income_white = db.Column(db.Float)
    median_property_value = db.Column(db.Float)
    pop_total = db.Column(db.Float)
    pop_18_24 = db.Column(db.Integer)
    pop_25_34 = db.Column(db.Integer)
    pop_35_44 = db.Column(db.Integer)
    pop_45_54 = db.Column(db.Integer)
    pop_55_64 = db.Column(db.Integer)
    pop_asian = db.Column(db.Integer)
    pop_black = db.Column(db.Integer)
    pop_gt_2 = db.Column(db.Integer)
    pop_gt_65 = db.Column(db.Integer)
    pop_hawaiian = db.Column(db.Integer)
    pop_hispanic = db.Column(db.Integer)
    pop_lt_18 = db.Column(db.Integer)
    pop_native = db.Column(db.Integer)
    pop_white = db.Column(db.Integer)
    pop_other = db.Column(db.Integer)
    universities = db.Column(db.ARRAY(db.String()))
    majors_and_populations = db.Column(db.JSON())
    majors_and_populations_longer = db.Column(db.JSON())
    images = db.Column(db.ARRAY(db.String()))


class Majors(db.Model):
    major = db.Column(db.String(), nullable=False, primary_key=True)
    area_of_study = db.Column(db.String(), nullable=False)
    description = db.Column(db.ARRAY(db.String()), nullable=False)
    wiki_link = db.Column(db.String(), nullable=False)
    youtube_link = db.Column(db.String, nullable=False)
    starting_salary = db.Column(db.Integer, nullable=False)
    cities = db.Column(db.JSON(), nullable=False)
    universities = db.Column(db.JSON(), nullable=False)
    images = db.Column(db.ARRAY(db.String()))


def university_main():
    with open("../scraping/university_scraper/university_info.json") as f:
        data = json.load(f)
        data = data["data"]

        with open("../scraping/city_scraper/codes.json") as f2:
            fips_data = json.load(f2)["data"]
            available_cities = create_city_dict(fips_data)

        for university in data:
            state = university["state"].replace(" ", "_")
            if not university["city"] in available_cities[state]:
                continue
            top_majors = get_top_majors(university["academics"], university["size"])

            result = requests.get(get_url() + university["name"] + " campus").json()
            time.sleep(3)
            my_images = parse_images(result)
            # my_images = []

            temp = Universities(
                school_id=university["id"],
                school_name=university["name"],
                school_alias=university["alias"],
                city=university["city"],
                abbrv_state=university["abbrv_state"],
                state=university["state"],
                school_url=university["school_url"],
                price_calculator_url=university["price_calculator_url"],
                latitude=university["latitude"],
                longitude=university["longitude"],
                undergrad_size=university["size"],
                race_ethnicity_white=university["race_ethnicity.white"],
                race_ethnicity_black=university["race_ethnicity.black"],
                race_ethnicity_hispanic=university["race_ethnicity.hispanic"],
                race_ethnicity_asian=university["race_ethnicity.asian"],
                race_ethnicity_aian=university["race_ethnicity.aian"],
                race_ethnicity_nhpi=university["race_ethnicity.nhpi"],
                race_ethnicity_two_or_more=university["race_ethnicity.two_or_more"],
                race_ethnicity_non_resident_alien=university[
                    "race_ethnicity.non_resident_alien"
                ],
                race_ethnicity_unknown=university["race_ethnicity.unknown"],
                graduation_rate=university["rate_suppressed_overall"],
                median_income=university["median_income"],
                percent_of_degrees=university["academics"],
                admission_rate=university["admission_rate"],
                sat_average=university["sat_average"],
                act_midpoint=university["act_midpoint"],
                in_state_tuition=university["in_state"],
                out_of_state_tuition=university["out_of_state"],
                private_tuition=university["private"],
                public_tuition=university["public"],
                top_majors=top_majors[:3],
                top_majors_longer=top_majors,
                images=my_images,
            )

            db.session.add(temp)
            db.session.commit()


def cities_main():
    with open("./../scraping/city_scraper/cities.json") as f:
        data = json.load(f)
    cities = data["data"]

    for city in cities:
        city_name = city["name"]
        state_abbrv = city["abbrv"]
        result = (
            Universities.query.filter_by(city=city_name, abbrv_state=state_abbrv)
            .with_entities(Universities.school_name)
            .all()
        )
        list_of_schools = get_list_of_schools_by_city(result)

        population_per_major = {}
        for school in list_of_schools:
            result = (
                Universities.query.filter_by(school_name=school)
                .with_entities(Universities.top_majors_longer)
                .all()
            )
            assert len(result) == 1

            majors = result[0][0]
            population_per_major = add_major_info_to_population_per_major(
                majors, population_per_major
            )

        population_per_major = sorted(
            population_per_major.items(), key=operator.itemgetter(1), reverse=True
        )
        sorted_population_per_major = get_sorted_population_per_major_for_database(
            population_per_major
        )

        result = requests.get(
            get_url() + city["name"] + " " + city["state"] + " cityscape"
        ).json()
        time.sleep(3)
        my_images = parse_images(result)
        # my_images = []

        new_city = Cities(
            name=city["name"],
            state=city["state"],
            abbrv_state=city["abbrv"],
            age=city["age"],
            income=city["income"],
            income_2ormore=city["income_2ormore"],
            income_asian=city["income_asian"],
            income_black=city["income_black"],
            income_hawaiian=city["income_hawaiian"],
            income_hispanic=city["income_hispanic"],
            income_native=city["income_native"],
            income_white=city["income_white"],
            median_property_value=city["median_property_value"],
            pop_total=city["pop_total"],
            pop_18_24=city["pop_18_24"],
            pop_25_34=city["pop_25_34"],
            pop_35_44=city["pop_35_44"],
            pop_45_54=city["pop_45_54"],
            pop_55_64=city["pop_55_64"],
            pop_asian=city["pop_asian"],
            pop_black=city["pop_black"],
            pop_gt_2=city["pop_gt_2"],
            pop_gt_65=city["pop_gt_65"],
            pop_hawaiian=city["pop_hawaiian"],
            pop_hispanic=city["pop_hispanic"],
            pop_lt_18=city["pop_lt_18"],
            pop_native=city["pop_native"],
            pop_white=city["pop_white"],
            pop_other=city["pop_other"],
            images=my_images,
            universities=list_of_schools,
            majors_and_populations=sorted_population_per_major[:3],
            majors_and_populations_longer=sorted_population_per_major,
        )

        db.session.add(new_city)

    db.session.commit()


def majors_main():
    result = Universities.query.with_entities(
        Universities.school_name, Universities.top_majors_longer
    ).all()

    with open("../scraping/majors_scraper/majors_info.json") as f:
        data = json.load(f)
        list_of_majors_files = data["data"]

        for major_filename in list_of_majors_files:
            description = []
            with open(
                "../scraping/majors_scraper/" + major_filename, encoding="utf-8"
            ) as major_file:
                for line in major_file:
                    description.append(line.strip())

            major_name = major_filename.replace("_parsed.txt", "")
            proper_major_name = major_name.title()

            school_and_major_percentage = get_school_and_major_percentage(
                result, proper_major_name
            )

            major_and_populations_from_cities = Cities.query.with_entities(
                Cities.name, Cities.majors_and_populations_longer, Cities.state
            ).all()
            cities_per_major = get_cities_per_major(
                major_and_populations_from_cities, proper_major_name
            )

            temp = Majors(
                major=proper_major_name,
                area_of_study=get_area_of_study_from_major(major_name),
                description=description,
                wiki_link="https://en.wikipedia.org/wiki/" + proper_major_name,
                youtube_link=get_youtube_link_per_major(major_name),
                starting_salary=get_salary_per_major(major_name),
                cities=cities_per_major[:3],
                universities=school_and_major_percentage[:3],
                images=get_image_link_per_major(major_name),
            )

            db.session.add(temp)
            db.session.commit()


def get_url():
    return (
        "https://www.googleapis.com/customsearch/v1?num=3&searchType=image&imgSize=large&cx=000671432870341"
        + "022161:ujzkoftvs7u&filetype=jpg&key="
        + key
        + "&q="
    )


def parse_images(result):
    data = result["items"]
    images = []
    for d in data:
        if "fbsbx" not in d["link"]:
            images.append(d["link"])
    return images


def get_top_majors(dict_of_fields, undergrad_size):
    top = []

    sorted_by_percentage = sorted(
        dict_of_fields.items(), key=operator.itemgetter(1), reverse=True
    )

    for field in sorted_by_percentage:
        if field[1] > 0:
            major = get_major_from_college_scorecard_area_of_study(field[0])
            if major is not None:
                number_in_major = field[1] * undergrad_size
                top.append(
                    {
                        "major": major,
                        "percentage_of_degrees": field[1],
                        "major_population": number_in_major,
                    }
                )

    return top


def get_list_of_schools_by_city(result):
    list_of_schools = []
    for school_name in result:
        list_of_schools.append(school_name[0])
    return list_of_schools


def add_major_info_to_population_per_major(majors, population_per_major):
    for dict_major in majors:
        major = dict_major["major"]
        population = dict_major["major_population"]
        if major in population_per_major:
            population_per_major[major] += population
        else:
            population_per_major[major] = population
    return population_per_major


def get_sorted_population_per_major_for_database(population_per_major):
    sorted_population_per_major = []
    for major_pop in population_per_major:
        temp = {}
        major = major_pop[0]
        population = major_pop[1]
        temp["major"] = major
        temp["major_population"] = population
        sorted_population_per_major.append(temp)
    return sorted_population_per_major


def get_school_and_major_percentage(result, proper_major_name):
    school_and_major_percentage = []
    for university in result:
        school_name = university[0]
        top_majors = university[1]
        for major_dict in top_majors:
            major = major_dict["major"]
            if major == proper_major_name:
                school_and_major_percentage.append(
                    {
                        "school_name": school_name,
                        "percentage_of_degrees": major_dict["percentage_of_degrees"],
                    }
                )

    school_and_major_percentage = sorted(
        school_and_major_percentage,
        key=lambda k: k["percentage_of_degrees"],
        reverse=True,
    )

    return school_and_major_percentage


def get_cities_per_major(major_and_populations_from_cities, proper_major_name):
    cities_per_major = []
    for city_major_combo in major_and_populations_from_cities:
        city_name = city_major_combo[0]
        major_dict = city_major_combo[1]
        city_state = city_major_combo[2]

        try:
            cities_per_major.append(
                {
                    "location": city_name + ", " + city_state,
                    "major_population": [
                        info_dict["major_population"]
                        for info_dict in major_dict
                        if proper_major_name == info_dict["major"]
                    ][0],
                }
            )
        except IndexError:
            pass

    return sorted(cities_per_major, key=lambda k: k["major_population"], reverse=True)


# kinesiology salary: https://work.chron.com/salaries-kinesiology-7626.html
# all other salaries: https://www.theloanmajor.com/starting-salaries/
# starting salaries based on 2016 data
def get_salary_per_major(major):
    dictionary = {
        "mathematics": 49864,
        "marketing": 42742,
        "performing arts": 36831,
        "computer science": 67355,
        "psychology": 36086,
        "political science": 44012,
        "architecture": 41930,
        "mechanical engineering": 57724,
        "history": 40014,
        "kinesiology": 36160,
        "biology": 35855,
        "communication studies": 39309,
        "social work": 31050,
    }

    return dictionary[major]


def get_youtube_link_per_major(major):
    dictionary = {
        "mathematics": "https://www.youtube.com/embed/GLMZc2EzsaI",
        "marketing": "https://www.youtube.com/embed/oytgLEwt3PY",
        "performing arts": "https://www.youtube.com/embed/jHdhvQ7Vdr0",
        "computer science": "https://www.youtube.com/embed/Tzl0ELY_TiM",
        "psychology": "https://www.youtube.com/embed/oRIjUBtKWyg",
        "political science": "https://www.youtube.com/embed/32fE9m6h07I",
        "architecture": "https://www.youtube.com/embed/UI98CKMVgRY",
        "mechanical engineering": "https://www.youtube.com/embed/W74y1RxN6BA",
        "history": "https://www.youtube.com/embed/7vIqfVw2wZw",
        "kinesiology": "https://www.youtube.com/embed/PHrhoMjCH7I",
        "biology": "https://www.youtube.com/embed/2o8LDbsZOh0",
        "communication studies": "https://www.youtube.com/embed/UvVi9XLc5jQ",
        "social work": "https://www.youtube.com/embed/D80oGYKbp_0",
    }

    return dictionary[major]


def get_image_link_per_major(major):
    dictionary = {
        "mathematics": [
            "https://www.jesus.cam.ac.uk/sites/default/files/styles/image/public/mathematics-936697_1920.jpg?itok=FProE_oh"
        ],
        "marketing": [
            "https://www.prestigeacademy.co.za/wp-content/uploads/2018/10/marketing-digital.png"
        ],
        "performing arts": [
            "https://performingarts.georgetown.edu/sites/performingarts/files/styles/hero_image/public/GUDC_Giselle_web.jpg"
        ],
        "computer science": [
            "https://www.uri.edu/programs/wp-content/uploads/programs/sites/3/2014/04/feat_img_comp_sci.jpg"
        ],
        "psychology": [
            "http://content.artshub.com.au/managedimages/content/2-273647-Main-600x450-7.jpg"
        ],
        "political science": [
            "https://images.shiksha.com/mediadata/images/articles/1508914325phptsBI3l.jpeg"
        ],
        "architecture": [
            "https://images.techhive.com/images/article/2015/06/lego_tower-100589775-primary.idge.jpg"
        ],
        "mechanical engineering": [
            "https://continuinged.uml.edu/images/2015/programs/online-mechanical-engineering-technology-bachelor-image.jpg"
        ],
        "history": [
            "https://cdn.theatlantic.com/assets/media/img/mt/2015/03/RossBetsy_1/lead_720_405.jpg?mod=1533691734"
        ],
        "kinesiology": [
            "https://www.rosevilleautoaccidentinjury.com/images/services/applied-kinesiology.jpg"
        ],
        "biology": [
            "http://blogs.biomedcentral.com/on-biology/wp-content/uploads/sites/5/2017/12/DNA.png"
        ],
        "communication studies": [
            "https://www.ccsf.edu/content/ccsf/en/educational-programs/school-and-departments/school-of-liberal-arts/communication/_jcr_content/contentparsys/imagebanner_122206721/image.img.jpg/1501705332452.jpg"
        ],
        "social work": [
            "https://edwardscampus.ku.edu/sites/edwardscampus.ku.edu/files/images/general/BSW-Bubble2.png"
        ],
    }

    return dictionary[major]


def make_json_all(instance, my_cls):
    my_json = {}
    for column in my_cls.__table__.columns:
        data = getattr(instance, column.name)
        my_json.update({column.name: data})
    return my_json


def make_json(instance, my_cls, cols):
    if cols is None:
        return make_json_all(instance, my_cls)
    my_json = {}
    for column in cols:
        data = getattr(instance, column)
        my_json.update({column: data})
    return my_json


def make_json_list(data, my_cls, cols):
    json_list = []
    for d in data:
        json_list.append(make_json(d, my_cls, cols))
    return json_list


def create_city_dict(data):
    my_dict = {}
    for state in data:
        cities = []
        for city in state["places"]:
            cities.append(city["name"])
        my_dict.update({state["state"]: cities})
    return my_dict


def sort_query(my_query, column_order, order):
    if column_order is None:
        return my_query.all()
    if order == "ascending":
        return my_query.order_by(column_order).all()
    if order == "descending":
        return my_query.order_by(desc(column_order)).all()
    return my_query.all()


def my_query_all(my_class, filters, column_order, order):
    filters = {key: value for key, value in filters.items() if value is not None}
    my_query = my_class.query.filter_by(**filters)
    return sort_query(my_query, column_order, order)


def convert_to_floats(my_list, to_replace, replace_with):
    if my_list is None:
        return []

    my_list = my_list.split(",")
    new_list = []
    for item in my_list:
        if item == to_replace:
            new_list.append(float(replace_with))
        else:
            new_list.append(float(item))

    return new_list


def parse_filters_and_mins_and_maxes(my_filters, mins, maxes):
    if my_filters is None:
        return [], mins, maxes
    mins = convert_to_floats(mins, "", "-inf")
    maxes = convert_to_floats(maxes, "", "inf")
    max_length = max(len(mins), len(maxes))
    mins += [float("-inf")] * (max_length - len(mins))
    maxes += [float("inf")] * (max_length - len(maxes))
    return my_filters.split(","), mins, maxes


def add_filters_to_query(my_class, query, filters, name, mins, maxes):
    index = 0
    for my_filter in filters:
        my_column = my_class.__table__.c[my_filter]
        my_type = my_column.type
        if name and isinstance(my_type, db.String):
            query = query.filter(my_column.ilike(name))
        elif isinstance(my_type, db.Float) or isinstance(my_type, db.Integer):
            query = query.filter(my_column.between(mins[index], maxes[index]))
            index += 1
    return query


def update_snippets(snippets, column, snippet):
    snippets.append({"column": column, "snippet": snippet})


def search_for_term(instances, simple, array, json, keys, snippets, term, special=None):
    kept = []
    for i in instances:
        found = search_simple_columns(i, simple, term, snippets)
        if not found:
            found = search_array_columns(i, array, term, snippets, special=special)
        if not found:
            found = search_json_columns(i, json, keys, term, snippets)
        if found:
            kept.append(i)
    return kept


def search_simple_columns(instance, columns, term, snippets):
    for col in columns:
        if re.match(term, instance[col], re.IGNORECASE):
            update_snippets(snippets, col, instance[col])
            return True
    return False


def search_array_columns(instance, columns, term, snippets, special=None):
    for col in columns:
        for inner in instance[col]:
            if re.match(term, inner, re.IGNORECASE):
                snippet = inner
                if col == "description":
                    snippet = re.search(
                        r"[A-Z]+[^\.]*" + special + r"[^\.]*\.", inner, re.IGNORECASE
                    ).group(0)
                update_snippets(snippets, col, snippet)
                return True
    return False


def search_json_columns(instance, columns, keys, term, snippets):
    for col, key in zip(columns, keys):
        for inner in instance[col]:
            if re.match(term, inner[key], re.IGNORECASE):
                update_snippets(snippets, col, inner[key])
                return True
    return False


@application.route("/cities", methods=["GET"])
def get_cities():
    req_city = request.args.get("city", type=str)
    req_state = request.args.get("state", type=str)
    req_columns = request.args.get("columns", type=str)
    column_order = request.args.get("order_by", type=str)
    order = request.args.get("order", default="ascending", type=str)

    if req_columns is not None:
        req_columns = req_columns.split(",")
    if req_city is not None:
        req_city = req_city.replace(" City", "")

    try:
        if not req_city or not req_state:
            cities = my_query_all(
                Cities, {"name": req_city, "state": req_state}, column_order, order
            )
            if len(cities) == 0:
                return jsonify(error="No cities found"), 404
            return jsonify(data=make_json_list(cities, Cities, req_columns)), 200
        else:
            city = None
            if len(req_state) > 2:
                city = Cities.query.filter_by(name=req_city, state=req_state).first()
            else:
                city = Cities.query.filter_by(
                    name=req_city, abbrv_state=req_state
                ).first()
            if city is None:
                return jsonify(error="No cities found"), 404
            return jsonify(make_json(city, Cities, req_columns)), 200
    except (AttributeError, exc.ProgrammingError, exc.CompileError):
        return jsonify(error="Invalid Column Name"), 404


@application.route("/universities", methods=["GET"])
def get_universities():
    req_uni = request.args.get("name", type=str)
    req_columns = request.args.get("columns", type=str)
    column_order = request.args.get("order_by", type=str)
    order = request.args.get("order", default="ascending", type=str)

    if req_columns is not None:
        req_columns = req_columns.split(",")

    try:
        if req_uni is None:
            universities = my_query_all(Universities, {}, column_order, order)
            return (
                jsonify(data=make_json_list(universities, Universities, req_columns)),
                200,
            )
        else:
            university = Universities.query.filter_by(school_name=req_uni).first()
            if university is None:
                return jsonify(error="No universities found"), 404
            return jsonify(make_json(university, Universities, req_columns)), 200
    except (AttributeError, exc.ProgrammingError, exc.CompileError):
        return jsonify(error="Invalid Column Name"), 404


@application.route("/majors", methods=["GET"])
def get_majors():
    req_major = request.args.get("major", type=str)
    req_field = request.args.get("field", type=str)
    column_order = request.args.get("order_by", type=str)
    order = request.args.get("order", default="ascending", type=str)

    if req_major is not None:
        major = Majors.query.filter_by(major=req_major).first()
        if major is None:
            return jsonify(error="No major found"), 404
        return jsonify(make_json_all(major, Majors)), 200

    try:
        majors = my_query_all(Majors, {"area_of_study": req_field}, column_order, order)
        if len(majors) == 0:
            return jsonify(error="No majors found"), 404
        return jsonify(data=make_json_list(majors, Majors, None)), 200
    except (AttributeError, exc.ProgrammingError, exc.CompileError):
        return jsonify(error="Invalid Column Name"), 404


@application.route("/search/cities", methods=["GET"])
def search_cities():
    search = request.args.get("q", type=str)
    column_order = request.args.get("order_by", type=str)
    order = request.args.get("order", default="ascending", type=str)
    my_filters = request.args.get("filters", type=str)
    state = request.args.get("state", type=str)
    mins = request.args.get("mins", type=str)
    maxes = request.args.get("maxes", type=str)

    my_filters, mins, maxes = parse_filters_and_mins_and_maxes(my_filters, mins, maxes)

    cities = Cities.query
    try:
        cities = add_filters_to_query(Cities, cities, my_filters, state, mins, maxes)
        cities = sort_query(cities, column_order, order)
        cities = make_json_list(cities, Cities, None)
        snippets = []
        if search:
            search = ".*" + search + ".*"
            simple_columns = ["name", "state", "abbrv_state"]
            array_columns = ["universities"]
            json_columns = ["majors_and_populations"]
            keys = ["major"]
            cities = search_for_term(
                cities,
                simple_columns,
                array_columns,
                json_columns,
                keys,
                snippets,
                search,
            )
        if len(cities) == 0:
            return jsonify(error="No cities found"), 404
        return jsonify(data=cities, snippets=snippets), 200
    except (AttributeError, exc.ProgrammingError, KeyError, exc.CompileError):
        return jsonify(error="Invalid Column Name"), 404
    except (IndexError):
        return jsonify(error="To few min-max pairs"), 404


@application.route("/search/universities", methods=["GET"])
def search_universities():
    search = request.args.get("q", type=str)
    column_order = request.args.get("order_by", type=str)
    order = request.args.get("order", default="ascending", type=str)
    my_filters = request.args.get("filters", type=str)
    state = request.args.get("state", type=str)
    mins = request.args.get("mins", type=str)
    maxes = request.args.get("maxes", type=str)

    my_filters, mins, maxes = parse_filters_and_mins_and_maxes(my_filters, mins, maxes)

    universities = Universities.query
    try:
        universities = add_filters_to_query(
            Universities, universities, my_filters, state, mins, maxes
        )
        universities = sort_query(universities, column_order, order)
        universities = make_json_list(universities, Universities, None)
        snippets = []
        if search:
            search = ".*" + search + ".*"
            simple_columns = ["school_name", "city", "state", "abbrv_state"]
            json_columns = ["top_majors"]
            keys = ["major"]
            universities = search_for_term(
                universities, simple_columns, [], json_columns, keys, snippets, search
            )
        if len(universities) == 0:
            return jsonify(error="No universities found"), 404
        return jsonify(data=universities, snippets=snippets), 200
    except (AttributeError, exc.ProgrammingError, KeyError, exc.CompileError):
        return jsonify(error="Invalid Column Name"), 404
    except (IndexError):
        return jsonify(error="To few min-max pairs"), 404


@application.route("/search/majors", methods=["GET"])
def search_majors():
    search = request.args.get("q", type=str)
    column_order = request.args.get("order_by", type=str)
    order = request.args.get("order", default="ascending", type=str)
    my_filters = request.args.get("filters", type=str)
    field = request.args.get("field", type=str)
    mins = request.args.get("mins", type=str)
    maxes = request.args.get("maxes", type=str)

    my_filters, mins, maxes = parse_filters_and_mins_and_maxes(my_filters, mins, maxes)

    majors = Majors.query
    try:
        majors = add_filters_to_query(Majors, majors, my_filters, field, mins, maxes)
        majors = sort_query(majors, column_order, order)
        majors = make_json_list(majors, Majors, None)
        snippets = []
        if search:
            unaltered = search
            search = ".*" + search + ".*"
            simple_columns = ["major", "area_of_study"]
            array_columns = ["description"]
            json_columns = ["universities", "cities"]
            keys = ["school_name", "location"]
            majors = search_for_term(
                majors,
                simple_columns,
                array_columns,
                json_columns,
                keys,
                snippets,
                search,
                special=unaltered,
            )

        if len(majors) == 0:
            return jsonify(error="No majors found"), 404
        return jsonify(data=majors, snippets=snippets), 200
    except (AttributeError, exc.ProgrammingError, KeyError, exc.CompileError):
        return jsonify(error="Invalid Column Name"), 404
    except (IndexError):
        return jsonify(error="To few min-max pairs"), 404


@application.route("/")
def landing_page():
    return (
        "<a href='https://documenter.getpostman.com/view/6755621/S11KQJok' target='_blank'><h2>Read our "
        + "API Documentation</h2><a>"
    )


def main():
    db.drop_all()
    db.create_all()
    university_main()
    cities_main()
    majors_main()
    application.run()


if __name__ == "__main__":
    main()
