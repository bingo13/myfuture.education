from application import (
    Universities,
    Cities,
    Majors,
    parse_images,
    get_top_majors,
    get_list_of_schools_by_city,
    add_major_info_to_population_per_major,
    get_school_and_major_percentage,
    get_cities_per_major,
    get_salary_per_major,
    get_youtube_link_per_major,
    make_json_all,
    make_json,
    make_json_list,
    create_city_dict,
    sort_query,
)
import majors_and_fields_translations as translations
from unittest import TestCase, main
import os
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, jsonify, request
import json


MAJORS_INSTANCE = Majors(
    major="Major",
    area_of_study="Area of Study",
    description=["p1", "p2", "p3"],
    wiki_link="https://en.wikipedia.org/wiki/Major",
    youtube_link="youtube.com",
    starting_salary=100000,
    cities={"city1": "some info", "city2": "some more info"},
    universities={"uni1": "info1", "uni2": "info2", "uni3": "info3"},
    images="link",
)


class TestApplication(TestCase):
    def test_parse_images1(self):
        json_data = {
            "dummy_data1": [],
            "dummy_data2": [],
            "items": [
                {"name": "name1", "link": "somepicture1.com", "not_link": []},
                {"name": "name2", "link": "somepicture2.com", "not_link": []},
            ],
            "dummy_data3": [],
        }
        self.assertEqual(
            parse_images(json_data), ["somepicture1.com", "somepicture2.com"]
        )

    def test_parse_images2(self):
        json_data = {
            "dummy_data1": [],
            "dummy_data2": [],
            "items": [
                {"name": "name1", "link": "somepicture1.fbsbx.com", "not_link": []},
                {"name": "name2", "link": "somepicture2.com", "not_link": []},
            ],
            "dummy_data3": [],
        }
        self.assertEqual(parse_images(json_data), ["somepicture2.com"])

    def test_get_top_majors1(self):
        dict_of_fields = {
            "mathematics": 0.0235,
            "business_marketing": 0.2263,
            "visual_performing": 0.0246,
            "computer": 0.0433,
            "psychology": 0.0281,
            "social_science": 0.0856,
            "communication": 0.091,
            "public_administration_social_service": 0.0074,
            "architecture": 0.0034,
            "engineering": 0.0968,
            "history": 0.0142,
            "physical_science": 0.0255,
            "biological": 0.0883,
        }

        undergrad_size = 100
        expected_value = [
            {
                "major": "Marketing",
                "percentage_of_degrees": 0.2263,
                "major_population": 22.63,
            },
            {
                "major": "Mechanical Engineering",
                "percentage_of_degrees": 0.0968,
                "major_population": 9.68,
            },
            {
                "major": "Communication Studies",
                "percentage_of_degrees": 0.091,
                "major_population": 9.1,
            },
            {
                "major": "Biology",
                "percentage_of_degrees": 0.0883,
                "major_population": 8.83,
            },
            {
                "major": "Political Science",
                "percentage_of_degrees": 0.0856,
                "major_population": 8.559999999999999,
            },
            {
                "major": "Computer Science",
                "percentage_of_degrees": 0.0433,
                "major_population": 4.33,
            },
            {
                "major": "Psychology",
                "percentage_of_degrees": 0.0281,
                "major_population": 2.81,
            },
            {
                "major": "Kinesiology",
                "percentage_of_degrees": 0.0255,
                "major_population": 2.55,
            },
            {
                "major": "Performing Arts",
                "percentage_of_degrees": 0.0246,
                "major_population": 2.46,
            },
            {
                "major": "Mathematics",
                "percentage_of_degrees": 0.0235,
                "major_population": 2.35,
            },
            {
                "major": "History",
                "percentage_of_degrees": 0.0142,
                "major_population": 1.4200000000000002,
            },
            {
                "major": "Social Work",
                "percentage_of_degrees": 0.0074,
                "major_population": 0.74,
            },
            {
                "major": "Architecture",
                "percentage_of_degrees": 0.0034,
                "major_population": 0.33999999999999997,
            },
        ]
        self.assertEqual(get_top_majors(dict_of_fields, undergrad_size), expected_value)

    def test_get_top_majors2(self):
        dict_of_fields = {
            "mathematics": 0.0235,
            "business_marketing": 0.2263,
            "communication": 0,
            "visual_performing": 0.0246,
            "fake_area": 0.0883,
        }
        undergrad_size = 100
        expected_value = [
            {
                "major": "Marketing",
                "percentage_of_degrees": 0.2263,
                "major_population": 22.63,
            },
            {
                "major": "Performing Arts",
                "percentage_of_degrees": 0.0246,
                "major_population": 2.46,
            },
            {
                "major": "Mathematics",
                "percentage_of_degrees": 0.0235,
                "major_population": 2.35,
            },
        ]
        self.assertEqual(get_top_majors(dict_of_fields, undergrad_size), expected_value)

    def test_get_list_of_schools_by_city(self):
        input_data = [
            ["University of Southern California", "Los Angeles", "California"],
            ["University of California-Los Angeles", "Los Angeles", "California"],
        ]

        expected_value = [
            "University of Southern California",
            "University of California-Los Angeles",
        ]

        self.assertEqual(get_list_of_schools_by_city(input_data), expected_value)

    def test_add_major_info_to_population_per_major1(self):
        majors = [{"major": "Major1", "major_population": 2}]
        population_per_major = {}

        expected_value = {"Major1": 2}

        self.assertEqual(
            add_major_info_to_population_per_major(majors, population_per_major),
            expected_value,
        )

    def test_add_major_info_to_population_per_major2(self):
        majors = [
            {"major": "Major2", "major_population": 3},
            {"major": "Major1", "major_population": 2},
        ]
        population_per_major = {"Major1": 2}

        expected_value = {"Major1": 4, "Major2": 3}

        self.assertEqual(
            add_major_info_to_population_per_major(majors, population_per_major),
            expected_value,
        )

    def test_add_major_info_to_population_per_major3(self):
        majors = [
            {"major": "Major2", "major_population": 5},
            {"major": "Major1", "major_population": 1},
            {"major": "Major3", "major_population": 0},
            {"major": "Major2", "major_population": 5},
        ]
        population_per_major = {"Major1": 4, "Major2": 3}

        expected_value = {"Major1": 5, "Major2": 13, "Major3": 0}

        self.assertEqual(
            add_major_info_to_population_per_major(majors, population_per_major),
            expected_value,
        )

    def test_get_school_and_major_percentage(self):
        input_data = [
            [
                "School1",
                [
                    {"major": "Major1", "percentage_of_degrees": 0.01},
                    {"major": "Major2", "percentage_of_degrees": 0.03},
                ],
            ],
            ["School2", [{"major": "Major1", "percentage_of_degrees": 0.05}]],
            [
                "School3",
                [
                    {"major": "Major2", "percentage_of_degrees": 0.02},
                    {"major": "Major3", "percentage_of_degrees": 0.05},
                ],
            ],
            [
                "School4",
                [
                    {"major": "Major2", "percentage_of_degrees": 0.06},
                    {"major": "Major3", "percentage_of_degrees": 0.07},
                    {"major": "Major4", "percentage_of_degrees": 0.08},
                ],
            ],
        ]
        proper_major_name = "Major2"

        expected_value = [
            {"school_name": "School4", "percentage_of_degrees": 0.06},
            {"school_name": "School1", "percentage_of_degrees": 0.03},
            {"school_name": "School3", "percentage_of_degrees": 0.02},
        ]

        self.assertEqual(
            get_school_and_major_percentage(input_data, proper_major_name),
            expected_value,
        )

    def test_get_cities_per_major1(self):
        major_and_populations_from_cities = [
            [
                "City1",
                [
                    {"major": "Major1", "major_population": 1000},
                    {"major": "Major2", "major_population": 4000},
                    {"major": "Major3", "major_population": 5000},
                ],
                "State1",
            ],
            [
                "City2",
                [
                    {"major": "Major2", "major_population": 2000},
                    {"major": "Major3", "major_population": 13000},
                    {"major": "Major4", "major_population": 4000},
                ],
                "State2",
            ],
            [
                "City3",
                [
                    {"major": "Major4", "major_population": 8000},
                    {"major": "Major3", "major_population": 9000},
                    {"major": "Major2", "major_population": 16000},
                ],
                "State3",
            ],
            [
                "City4",
                [
                    {"major": "Major1", "major_population": 23000},
                    {"major": "Major2", "major_population": 4000},
                ],
                "State4",
            ],
            ["City5", [{"major": "Major3", "major_population": 1000}], "State5"],
        ]
        proper_major_name = "Major3"

        expected_value = [
            {"location": "City2, State2", "major_population": 13000},
            {"location": "City3, State3", "major_population": 9000},
            {"location": "City1, State1", "major_population": 5000},
            {"location": "City5, State5", "major_population": 1000},
        ]

        self.assertEqual(
            get_cities_per_major(major_and_populations_from_cities, proper_major_name),
            expected_value,
        )

    def test_get_cities_per_major2(self):
        major_and_populations_from_cities = [
            [
                "City1",
                [
                    {"major": "Major1", "major_population": 1000},
                    {"major": "Major2", "major_population": 4000},
                    {"major": "Major3", "major_population": 5000},
                ],
                "State1",
            ],
            [
                "City2",
                [
                    {"major": "Major2", "major_population": 2000},
                    {"major": "Major3", "major_population": 13000},
                    {"major": "Major4", "major_population": 4000},
                ],
                "State2",
            ],
            [
                "City3",
                [
                    {"major": "Major4", "major_population": 8000},
                    {"major": "Major3", "major_population": 9000},
                    {"major": "Major2", "major_population": 16000},
                ],
                "State3",
            ],
            [
                "City4",
                [
                    {"major": "Major1", "major_population": 23000},
                    {"major": "Major2", "major_population": 4000},
                ],
                "State4",
            ],
            ["City5", [{"major": "Major3", "major_population": 1000}], "State5"],
        ]
        proper_major_name = "Major6"

        expected_value = []

        self.assertEqual(
            get_cities_per_major(major_and_populations_from_cities, proper_major_name),
            expected_value,
        )

    def test_get_salary_per_major1(self):
        self.assertEqual(get_salary_per_major("computer science"), 67355)

    def test_get_salary_per_major2(self):
        self.assertEqual(get_salary_per_major("social work"), 31050)

    def test_get_youtube_link_per_major1(self):
        self.assertEqual(
            get_youtube_link_per_major("computer science"),
            "https://www.youtube.com/embed/Tzl0ELY_TiM",
        )

    def test_get_youtube_link_per_major2(self):
        self.assertEqual(
            get_youtube_link_per_major("social work"),
            "https://www.youtube.com/embed/D80oGYKbp_0",
        )

    def test_make_json_all(self):
        global MAJORS_INSTANCE
        expected_value = {
            "major": "Major",
            "area_of_study": "Area of Study",
            "description": ["p1", "p2", "p3"],
            "wiki_link": "https://en.wikipedia.org/wiki/Major",
            "youtube_link": "youtube.com",
            "starting_salary": 100000,
            "cities": {"city1": "some info", "city2": "some more info"},
            "universities": {"uni1": "info1", "uni2": "info2", "uni3": "info3"},
            "images": "link",
        }
        self.assertEqual(make_json_all(MAJORS_INSTANCE, Majors), expected_value)

    def test_make_json(self):
        global MAJORS_INSTANCE
        cols = ["major", "area_of_study"]
        self.assertEqual(
            make_json(MAJORS_INSTANCE, Majors, cols),
            {"major": "Major", "area_of_study": "Area of Study"},
        )

    def test_make_json_list1(self):
        global MAJORS_INSTANCE
        cols = ["major", "area_of_study"]
        expected_value = [
            {"major": "Major", "area_of_study": "Area of Study"},
            {"major": "Major", "area_of_study": "Area of Study"},
        ]
        self.assertEqual(
            make_json_list([MAJORS_INSTANCE, MAJORS_INSTANCE], Majors, cols),
            expected_value,
        )

    def test_make_json_list2(self):
        global MAJORS_INSTANCE
        cols = None
        expected_value = [
            {
                "major": "Major",
                "area_of_study": "Area of Study",
                "description": ["p1", "p2", "p3"],
                "wiki_link": "https://en.wikipedia.org/wiki/Major",
                "youtube_link": "youtube.com",
                "starting_salary": 100000,
                "cities": {"city1": "some info", "city2": "some more info"},
                "universities": {"uni1": "info1", "uni2": "info2", "uni3": "info3"},
                "images": "link",
            },
            {
                "major": "Major",
                "area_of_study": "Area of Study",
                "description": ["p1", "p2", "p3"],
                "wiki_link": "https://en.wikipedia.org/wiki/Major",
                "youtube_link": "youtube.com",
                "starting_salary": 100000,
                "cities": {"city1": "some info", "city2": "some more info"},
                "universities": {"uni1": "info1", "uni2": "info2", "uni3": "info3"},
                "images": "link",
            },
        ]
        self.assertEqual(
            make_json_list([MAJORS_INSTANCE, MAJORS_INSTANCE], Majors, cols),
            expected_value,
        )

    def test_create_city_dict(self):
        test_data = [
            {
                "state": "Texas",
                "places": [
                    {"name": "Austin"},
                    {"name": "San Antonio"},
                    {"name": "Houston"},
                    {"name": "El Paso"},
                    {"name": "Dallas"},
                ],
            },
            {"state": "Arkansas", "places": [{"name": "Little Rock"}]},
            {"state": "Rhode Island", "places": []},
        ]
        expected_value = {
            "Texas": ["Austin", "San Antonio", "Houston", "El Paso", "Dallas"],
            "Arkansas": ["Little Rock"],
            "Rhode Island": [],
        }
        self.assertEqual(create_city_dict(test_data), expected_value)

    def test_database_values(self):
        postgres_local = os.environ.get("POSTGRES_LOCAL")

        # this should only run locally, not on GitLab CI
        if postgres_local is not None:
            application = Flask(__name__)
            CORS(application, resources={r"/*": {"origins": "*"}})
            application.config["DEBUG"] = True

            application.config["SQLALCHEMY_DATABASE_URI"] = (
                "postgresql://postgres:" + postgres_local + "@localhost:5432/postgres"
            )

            with open("query_test_results.json", "w+", encoding="utf-8") as actual_file:
                data = [
                    {
                        "Universities_data": Universities.query.with_entities(
                            Universities.school_id,
                            Universities.school_name,
                            Universities.city,
                            Universities.abbrv_state,
                            Universities.state,
                            Universities.undergrad_size,
                            Universities.percent_of_degrees,
                            Universities.top_majors,
                            Universities.top_majors_longer,
                        ).all()
                    },
                    {
                        "Cities_data": Cities.query.with_entities(
                            Cities.id,
                            Cities.name,
                            Cities.state,
                            Cities.abbrv_state,
                            Cities.universities,
                            Cities.majors_and_populations,
                            Cities.majors_and_populations_longer,
                        ).all()
                    },
                    {
                        "Majors_data": Majors.query.with_entities(
                            Majors.major,
                            Majors.area_of_study,
                            Majors.description,
                            Majors.wiki_link,
                            Majors.youtube_link,
                            Majors.starting_salary,
                            Majors.cities,
                            Majors.universities,
                        ).all()
                    },
                ]

                json.dump(data, actual_file, indent=4)

            with open("query_results.json", encoding="utf-8") as expected_file:
                with open("query_test_results.json", encoding="utf-8") as actual_file:
                    for actual_line, expected_line in zip(actual_file, expected_file):
                        self.assertEqual(actual_line, expected_line)

            os.remove("query_test_results.json")


class TestMajorsAndFields(TestCase):
    def test_get_major_from_college_scorecard_area_of_study1(self):
        self.assertEqual(
            translations.get_major_from_college_scorecard_area_of_study("mathematics"),
            "Mathematics",
        )

    def test_get_major_from_college_scorecard_area_of_study2(self):
        self.assertEqual(
            translations.get_major_from_college_scorecard_area_of_study(
                "physical_science"
            ),
            "Kinesiology",
        )

    def test_get_major_from_college_scorecard_area_of_study3(self):
        self.assertIsNone(
            translations.get_major_from_college_scorecard_area_of_study("education")
        )

    def test_get_area_of_study_from_major1(self):
        self.assertEqual(
            translations.get_area_of_study_from_major("mathematics"), "Science"
        )

    def test_get_area_of_study_from_major2(self):
        self.assertEqual(
            translations.get_area_of_study_from_major("kinesiology"), "Science"
        )

    def test_get_area_of_study_from_major3(self):
        self.assertEqual(
            translations.get_area_of_study_from_major("social work"), "Social Work"
        )


if __name__ == "__main__":
    main()
