def get_major_from_college_scorecard_area_of_study(area_of_study):
    dictionary = {
        "mathematics": "Mathematics",
        "business_marketing": "Marketing",
        "visual_performing": "Performing Arts",
        "computer": "Computer Science",
        "psychology": "Psychology",
        "social_science": "Political Science",
        "architecture": "Architecture",
        "engineering": "Mechanical Engineering",
        "history": "History",
        "physical_science": "Kinesiology",
        "biological": "Biology",
        "communication": "Communication Studies",
        "public_administration_social_service": "Social Work",
    }
    try:
        result = dictionary[area_of_study]
    except KeyError:
        result = None

    return result


def get_area_of_study_from_major(major):
    dictionary = {
        "mathematics": "Science",
        "marketing": "Business",
        "performing arts": "Fine Arts",
        "computer science": "Science",
        "psychology": "Liberal Arts",
        "political science": "Liberal Arts",
        "architecture": "Architecture",
        "mechanical engineering": "Engineering",
        "history": "Liberal Arts",
        "kinesiology": "Science",
        "biology": "Science",
        "communication studies": "Communication",
        "social work": "Social Work",
    }

    return dictionary[major]
