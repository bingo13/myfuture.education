import React from 'react'
import { Switch, Route } from 'react-router-dom'
import About from '../Pages/About/About'
import App from '../App'
import Cities from '../Pages/Cities/Cities'
import CitiesInstance from '../Pages/Cities/CitiesInstance'
import Majors from '../Pages/Majors/Majors'
import Universities from '../Pages/Universities/Universities'
import UniversitiesInstance from '../Pages/Universities/UniversitiesInstance';
import MajorsInstance from '../Pages/Majors/MajorsInstance';
import Search from '../Pages/Search/Search';
import Visualizations from '../Pages/Visualizations/Visualizations'


const Main = () => (
  <Switch>
    <Route exact path='/' component={App} />
    <Route exact path='/about' component={About} />
    <Route exact path='/search/:query' component={Search}/>
    <Route exact path='/cities' component={Cities} />
    <Route exact path='/majors' component={Majors} />
    <Route exact path='/cities/:city' component={CitiesInstance} />
    <Route exact path='/universities/:university' component={UniversitiesInstance} />
    <Route exact path='/majors/:major' component={MajorsInstance} />
    <Route exact path='/universities' component={Universities} />
    <Route exact path='/visualizations' component={Visualizations} />
  </Switch>
)

export default Main
