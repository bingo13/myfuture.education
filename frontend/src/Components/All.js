import React, { Component } from 'react'
import NavBar from './NavBar'
import Main from './Main'
import Footer from './Footer'

class All extends Component {

  render() {
    return (
      <div style={{ position: 'relative', minHeight: '100vh', padding: '100px 0px 60px 0px' }}>
        <NavBar />
        <Main />
        <Footer />
      </div>
    )
  }
}

export default All
