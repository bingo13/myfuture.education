import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import SchoolIcon from '@material-ui/icons/School';
import { Link, NavLink } from 'react-router-dom';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import InputAdornment from '@material-ui/core/InputAdornment';
import { IconButton, Grid } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import Search from '@material-ui/icons/Search';



const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  home: {
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  inner: {
    display: 'inline-flex',
    cursor: 'pointer'
  },
  icon: {
    fontSize: '30px',
    marginRight: '10px',
  },
  link: {
    color: '#fff',
    textDecoration: 'none !important',
    marginRight: '25px',
    '&:hover': {
      color: '#9778ce'
    }
  },
  main: {
    color: '#fff',
    textDecoration: 'none !important',
    '&:hover': {
      color: '#fff'
    }
  },
  selected: {
    fontWeight: 'bold',
    color: '#583c87'
  },
  textField: {
    marginRight: theme.spacing.unit,
    marginTop: '-5px',
  },
  cssLabel: {
    color: '#FFFFFF',
    '&$cssFocused': {
      color: '#FFFFFF',
    },
  },
  cssFocused: {},
  underline: {
    '&:before': {
      borderBottom: '1px solid rgba(255, 255, 255, 1)'
    },
    '&:after': {
        borderBottom: `1px solid ${theme.palette.secondary.main}`
    },
    '&:hover:before': {
        borderBottom: `1px solid ${theme.palette.secondary.main}`
    }
  },
  searchIcon: {
    color: "#FFFFFF",
    outline: 'none !important'
  }
});

class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchQuery: ''
    };
  }

  render() {
    const { classes, width } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="absolute">
          <Toolbar>
            <Grid container>
              <Grid item xs={12} sm={6}>
                <div className={classes.home}>
                  <Link className={classes.main} to='/' id='homepageHeaderLink'>
                    <div className={classes.inner}>
                      <SchoolIcon className={classes.icon} />
                      <Typography variant="h5" color="inherit">
                        My Future Education
                      </Typography>
                    </div>
                  </Link>
                </div>
              </Grid>

              <Grid item xs={12} sm={6}>
                <div style={{display: "flex", justifyContent: "flex-end"}}>
                    <NavLink to='/universities' className={classes.link} activeClassName={classes.selected} id='universitiesHeaderLink'>
                      <Typography variant="h6" color="inherit">Universities</Typography>
                    </NavLink>
                    <NavLink className={classes.link} to='/cities' activeClassName={classes.selected} id='citiesHeaderLink'>
                      <Typography variant="h6" color="inherit">Cities</Typography>
                    </NavLink>
                    <NavLink className={classes.link} to='/majors' activeClassName={classes.selected} id='majorsHeaderLink'>
                      <Typography variant="h6" color="inherit">Majors</Typography>
                    </NavLink>
                    <NavLink className={classes.link} to='/about' activeClassName={classes.selected} id='aboutHeaderLink'>
                      <Typography variant="h6" color="inherit">About</Typography>
                    </NavLink>
                    <NavLink className={classes.link} to='/visualizations' activeClassName={classes.selected} id='visualizationsHeaderLink'>
                      <Typography variant="h6" color="inherit">Visualizations</Typography>
                    </NavLink>
                    <TextField
                    className={classes.textField}
                    id="global-search"
                    placeholder="Search..."
                    margin="normal"
                    autoComplete="false"
                    onChange={(e) => {this.setState({searchQuery: e.target.value.trim()})}}
                    onKeyDown={(e) => {if (e.keyCode ===13 && this.state.searchQuery != "") {this.props.history.push(`/search/${this.state.searchQuery}`); window.location.reload()}}}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton className={classes.searchIcon} onClick={(e) => { if (this.state.searchQuery) { this.props.history.push(`/search/${this.state.searchQuery}`); window.location.reload()}}}>
                            <Search />
                          </IconButton>
                        </InputAdornment>
                      ),
                      classes: {
                        root: classes.cssLabel,
                        underline: classes.cssFocused,
                      },
                    }}
                    />
                </div>
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default compose(withRouter, withStyles(styles, {name: 'NavBar'}), withWidth())(NavBar);