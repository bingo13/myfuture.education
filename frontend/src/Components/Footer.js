import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon'
import Logo from '../Images/gitlab_logo.png'
import { Grid } from '@material-ui/core'
import { compose } from 'redux';


const styles = {
  root: {
    flexGrow: 1,
  },
  footer: {
    top: 'auto',
    bottom: 0,
    position: 'absolute',
    backgroundColor: '#333333'
  },
};

function Footer(props) {
  const { classes, width } = props;
  return (
    <div className={classes.root}>
      <AppBar color='primary' className={classes.footer} style={{paddingTop: width === 'xs' ? "1%" : "0", paddingBottom: width === 'xs' ? "1%" : "0"}}>
        <Toolbar>
          <Grid container spacing={8}>
            <Grid item xs={12} sm={6} justify="flex-end">
              <Typography variant={width === 'xs' ? "subheading" : "h6"} color="inherit" style={{textAlign: width === 'xs' ? "center" : "start"}}>
                Made with&nbsp;<Icon style={{ fontSize: '15px' }} color='secondary'>favorite</Icon>
                &nbsp;by SWE-Tea
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <div style={{display: "flex", justifyContent: width ===  'xs' ? "center" : "flex-end"}}>
                <a href='https://gitlab.com/bingo13/myfuture.education' target='_blank' rel='noopener noreferrer' id='gitlabFooterLink'>
                  <img src={Logo} height={width === 'xs' ? "20px" : "24px"} alt='GitLab logo'/>
                </a>
                <Typography variant={width === 'xs' ? "body1" : "subtitle1"} color="inherit" >&nbsp;• My Future Education © 2019</Typography>
              </div>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
  width: PropTypes.string.isRequired,
};

export default compose(withStyles(styles, {name: 'Footer'}), withWidth())(Footer);