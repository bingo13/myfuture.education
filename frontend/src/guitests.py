from selenium import webdriver
from unittest import main, TestCase
import time

class GuiTests(TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()

    def test_load_homepage(self):
        self.driver.get('https://myfuture.education')
        self.assertEqual(self.driver.title, 'My Future Education')

    def test_gitlab_footnote(self):
        self.driver.get('https://myfuture.education')
        gitlabelem = self.driver.find_element_by_id('gitlabFooterLink')
        gitlabelem.click()
        time.sleep(1)
        self.assertEqual(2, len(self.driver.window_handles))
        self.driver.close()
        self.driver.switch_to.window(self.driver.window_handles[0])

    def test_aboutNav_loads(self):
        self.driver.get('https://myfuture.education')
        navelem = self.driver.find_element_by_id('aboutHeaderLink')
        navelem.click()
        self.assertEqual(self.driver.title, 'About | My Future Education')

    def test_homeNav_loads(self):
        self.driver.get('https://myfuture.education/about')
        navelem = self.driver.find_element_by_id('homepageHeaderLink')
        navelem.click()
        self.assertEqual(self.driver.title, 'My Future Education')

    def test_majorsNav_loads(self):
        self.driver.get('https://myfuture.education')
        navelem = self.driver.find_element_by_id('majorsHeaderLink')
        navelem.click()
        self.assertEqual(self.driver.title, 'Majors | My Future Education')

    def test_universitiesNav_loads(self):
        self.driver.get('https://myfuture.education')
        navelem = self.driver.find_element_by_id('universitiesHeaderLink')
        navelem.click()
        self.assertEqual(self.driver.title, 'Universities | My Future Education')

    def test_citiesNav_loads(self):
        self.driver.get('https://myfuture.education')
        navelem = self.driver.find_element_by_id('citiesHeaderLink')
        navelem.click()
        self.assertEqual(self.driver.title, 'Cities | My Future Education')

    def test_cityInstance_loads(self):
        self.driver.get('https://myfuture.education/cities')
        time.sleep(1)
        cardelem = self.driver.find_elements_by_class_name('my-card')
        cardelem[0].click()
        self.assertIn('myfuture.education/cities/', self.driver.current_url)

    def test_universityInstance_loads(self):
        self.driver.get('https://myfuture.education/universities')
        time.sleep(1)
        cardelem = self.driver.find_elements_by_class_name('my-card')
        cardelem[0].click()
        self.assertIn('myfuture.education/universities/', self.driver.current_url)

    def test_majorInstance_loads(self):
        self.driver.get('https://myfuture.education/majors')
        time.sleep(1)
        cardelem = self.driver.find_elements_by_class_name('my-card')
        cardelem[0].click()
        self.assertIn('myfuture.education/majors/', self.driver.current_url)

    def test_link_from_university_to_city(self):
        self.driver.get('https://myfuture.education/universities')
        time.sleep(1)
        cardelems = self.driver.find_elements_by_class_name('my-card')
        cardelems[0].click()
        time.sleep(1)
        linkelems = self.driver.find_elements_by_class_name('Card-Media')
        linkelems[0].click()
        self.assertIn('myfuture.education/cities/', self.driver.current_url)

    def test_link_from_city_to_university(self):
        self.driver.get('https://myfuture.education/cities')
        time.sleep(1)
        cardelems = self.driver.find_elements_by_class_name('my-card')
        cardelems[0].click()
        time.sleep(1)
        linkelems = self.driver.find_elements_by_class_name('Card-Media')
        linkelems[0].click()
        self.assertIn('myfuture.education/universities/', self.driver.current_url)

    def test_link_from_major_to_city(self):
        self.driver.get('https://myfuture.education/majors')
        time.sleep(1)
        cardelems = self.driver.find_elements_by_class_name('my-card')
        cardelems[1].click()
        time.sleep(1)
        linkelems = self.driver.find_elements_by_class_name('Card-Media')
        linkelems[0].click()
        self.assertIn('myfuture.education/cities/', self.driver.current_url)

    def test_search_universities(self):
        self.driver.get('https://myfuture.education/universities')
        time.sleep(1)
        searchelem = self.driver.find_element_by_id('outlined-bare')
        searchelem.send_keys('Texas')
        searchelem.send_keys(u'\ue007')
        time.sleep(1)
        linkelems = self.driver.find_elements_by_class_name('my-card')
        linkelems[0].click()
        time.sleep(1)
        self.assertIn('myfuture.education/universities/Texas', self.driver.current_url)

    def test_search_cities(self):
        self.driver.get('https://myfuture.education/cities')
        time.sleep(1)
        searchelem = self.driver.find_element_by_id('outlined-bare')
        searchelem.send_keys('Austin')
        searchelem.send_keys(u'\ue007')
        time.sleep(1)
        linkelems = self.driver.find_elements_by_class_name('my-card')
        linkelems[0].click()
        time.sleep(1)
        self.assertIn('myfuture.education/cities/Austin_Texas', self.driver.current_url)

    def test_search_majors(self):
        self.driver.get('https://myfuture.education/majors')
        time.sleep(1)
        searchelem = self.driver.find_element_by_id('outlined-bare')
        searchelem.send_keys('science')
        searchelem.send_keys(u'\ue007')
        time.sleep(1)
        linkelems = self.driver.find_elements_by_class_name('my-card')
        linkelems[8].click()
        time.sleep(1)
        self.assertIn('myfuture.education/majors/Kinesiology', self.driver.current_url)

    def test_search_overall_universities(self):
        self.driver.get('https://myfuture.education/')
        time.sleep(1)
        searchelem = self.driver.find_element_by_id('global-search')
        searchelem.send_keys('texas')
        searchelem.send_keys(u'\ue007')
        time.sleep(1)
        linkelems = self.driver.find_elements_by_class_name('my-card')
        linkelems[0].click()
        time.sleep(1)
        self.assertIn('myfuture.education/universities/Texas', self.driver.current_url)

    def test_search_overall_majors(self):
        self.driver.get('https://myfuture.education/')
        time.sleep(1)
        searchelem = self.driver.find_element_by_id('global-search')
        searchelem.send_keys('texas')
        searchelem.send_keys(u'\ue007')
        time.sleep(1)
        linkelems = self.driver.find_elements_by_class_name('my-card')
        linkelems[6].click()
        time.sleep(1)
        self.assertIn('myfuture.education/majors/Computer', self.driver.current_url)
        
    def test_search_overall_cities(self):
        self.driver.get('https://myfuture.education/')
        time.sleep(1)
        searchelem = self.driver.find_element_by_id('global-search')
        searchelem.send_keys('texas')
        searchelem.send_keys(u'\ue007')
        time.sleep(1)
        linkelems = self.driver.find_elements_by_class_name('my-card')
        linkelems[3].click()
        time.sleep(1)
        self.assertIn('myfuture.education/cities/Austin', self.driver.current_url)

    def tearDown(self):
        self.driver.close()
