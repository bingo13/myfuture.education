import React, { Component } from 'react';
import './App.css';
import Carousel from 'react-bootstrap/Carousel'
import graduation from './Images/graduation.jpg';
import austin from './Images/austin.jpg';
import subject from './Images/subject.jpg';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom'
import city from './Images/city.png'
import university from './Images/university.png'
import major from './Images/majors.png'

class App extends Component {
  componentDidMount() {
    document.title = 'My Future Education'
  }

  render() {
    return (
      <div>
        <div>
          <Carousel className="Carousel">
            
            <Carousel.Item>
            <Link to='/universities'>
              <img
                className="Carousel-image"
                src={graduation}
                alt="University"
              />
              </Link>

              <Carousel.Caption>
                <Typography variant="h4" color="inherit">Find your dream university</Typography>
              </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
            <Link to='/cities'>
              <img
                className="Carousel-image"
                src={austin}
                alt="City"
              />
              </Link>

              <Carousel.Caption>
                <Typography variant="h4" color="inherit">Choose a city you love</Typography>
              </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
            <Link to='/majors'>
              <img
                className="Carousel-image"
                src={subject}
                alt="Subjects"
              />
              </Link>

              <Carousel.Caption>
                <Typography variant="h4" color="inherit">Explore various subjects</Typography>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
        </div>
        <div>
        <section className="bg-white text-center">
              <Typography variant="h4" color="default">Welcome!</Typography>
              <Typography variant="subtitle" color="action">My Future Education is a tool 
              to help prospective college students design a future that matches their 
              priorities. From choosing a university, city, and major, make this big decision
              with confidence.</Typography>
              <br></br>
              <div className="container">
                <div className="row">
                  <div className="col-lg-4">
                        <a href="/universities">
                        <img class="img-fluid" src={university} alt="" />
                        </a>
                      <h3 style={{textTransform: 'none', fontSize: '160%'}}>Universities</h3>
                      <p className="lead mb-0" style={{fontSize: '120%'}}>Compare universities from all over the US</p>

                  </div>

                  <div className="col-lg-4">
                        <a href="/cities">
                        <img class="img-fluid" src={city} alt="" />
                        </a>
                      <h3 style={{textTransform: 'none', fontSize: '160%'}}>Cities</h3>
                      <p className="lead mb-0" style={{fontSize: '120%'}}>Find the right city for you</p>

                  </div>

                  <div className="col-lg-4">
                        <a href="/majors">
                        <img class="img-fluid" src={major} alt="" />
                        </a>
                      <h3 style={{textTransform: 'none', fontSize: '160%'}}>Majors</h3>
                      <p className="lead mb-0" style={{fontSize: '120%'}}>Learn about majors to reach your potential</p>

                  </div>

                </div>
              </div>
            </section>
            </div>
      </div>
    );
  }
}

export default App;
