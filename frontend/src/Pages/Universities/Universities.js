import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import { CardActionArea, CardMedia, Typography, CardContent, Grid } from '@material-ui/core';
import './Universities.css';
import Pagination from 'material-ui-flat-pagination';
import axios from 'axios'
import TextField from '@material-ui/core/TextField';
import Search from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import { IconButton } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Highlighter from "react-highlight-words";

const universitiesUrl = 'https://api.myfuture.education/universities';
const universitiesSearchUrl = 'https://api.myfuture.education/search/universities';
const states = ["Alabama", "Alaska", "Arizona", "California", "Colorado", 
                "Connecticut", "District of Columbia", "Florida", "Georgia", 
                "Hawaii", "Illinois", "Indiana", "Iowa", "Louisiana", 
                "Maryland", "Massachusetts", "Michigan", "Minnesota", 
                "Mississippi", "New York", "North Carolina", "Ohio", 
                "Oklahoma", "Pennsylvania", "Tennessee", "Texas", "Utah", 
                "Virginia", "Washington", "Wisconsin"]

const rangesPopulation = [{min: "", max: "1000"}, 
                {min: "1001", max: "10000"}, 
                {min: "10001", max: "20000"}, 
                {min: "20001", max: "30000"}, 
                {min: "30001", max: "40000"},
                {min: "40001", max: ""}];

const rangesAdmission = [{min: "", max: "0.2"}, 
                {min: "0.21", max: "0.4"}, 
                {min: "0.41", max: "0.6"}, 
                {min: "0.61", max: "0.8"}, 
                {min: "0.81", max: ""}];

class Universities extends Component {

  constructor(props) {
    super(props)
    this.state = {
      offset: 0,
      universities: null,
      snippets: [],
      loaded: false,
      sort_by: "",
      state: "",
      population: "",
      admission_rate: "",
      params: {},
      filters: [],
      mins:[],
      maxes:[],
      error: "",
      searchQuery: ''
    }
  }


  componentDidMount() {
    axios.get(universitiesUrl).then((response) => {
      this.setState({universities: response.data.data}, () =>
        this.setState({loaded: true}))
      }).catch((error) =>
      this.setState({universities: [], error: "No universities found"})
    )


    document.title = 'Universities | My Future Education';
  }

  handleClick(offset) {
    this.setState({offset})
    window.scrollTo(0, 0)
  }

  end() {
    return this.state.offset + 9 >= this.state.universities.length ? this.state.universities.length : this.state.offset + 9;
  }

  getColumnName(column) {
    column = column.replace(new RegExp('_', 'g'), ' ');
    return column.charAt(0).toUpperCase() + column.substr(1).toLowerCase()
  }

  search = (event, button) => {
    if (button === 1 || event.keyCode === 13) {
      this.state.params.q=this.state.searchQuery;
      this.makeRequest();
    }
  }

  sortBy = event => {
    this.setState({[event.target.name]: event.target.value});
    this.state.params.order_by = 'school_name';
    this.state.params.order = event.target.value === 0 ? 'ascending' : 'descending'
    this.makeRequest();
  }

  filterPopulation = event => {
    this.setState({[event.target.name]: event.target.value});
    delete this.state.params.min;
    delete this.state.params.max;
    var range = event.target.value !== -1 ? rangesPopulation[event.target.value] : {min: "", max: ""}
    var idx = this.state.filters.indexOf('undergrad_size');
    var state_idx = this.state.filters.indexOf('state');
    var num_idx = idx

    if (state_idx !== -1 && num_idx > state_idx){
      num_idx = num_idx - 1
    }

    if (event.target.value === -1 && idx !== -1) {
      this.state.filters.splice(idx, 1);
      this.state.mins.splice(num_idx, 1);
      this.state.maxes.splice(num_idx, 1);
    } else {
      if (idx === -1) {
        this.state.filters.push('undergrad_size')
        this.state.mins.push(range.min)
        this.state.maxes.push(range.max)
      } else {
        this.state.filters.splice(idx, 1);
        this.state.mins.splice(num_idx, 1);
        this.state.maxes.splice(num_idx, 1);
        this.state.filters.push('undergrad_size')
        this.state.mins.push(range.min)
        this.state.maxes.push(range.max)
      }
    }
    if (this.state.filters.length > 0) {
      this.state.params.filters = this.state.filters.join(',');
      this.state.params.mins = this.state.mins.join(',');
      this.state.params.maxes = this.state.maxes.join(',');
    } else  {
      delete this.state.params.filters;
      delete this.state.params.mins;
      delete this.state.params.maxes;
    }
    this.makeRequest()
  };

  filterState = event => {
    this.setState({[event.target.name]: event.target.value});
    var idx = this.state.filters.indexOf('state');
    if (event.target.value === -1) {
      delete this.state.params.state;
      if (idx !== -1) {
        this.state.filters.splice(idx, 1);
      }
    } else {
      this.state.params.state=event.target.value;
      if (idx === -1) {
        this.state.filters.push('state');
      }
    }
    if (this.state.filters.length > 0) {
      this.state.params.filters = this.state.filters.join(',');
    } else  {
      delete this.state.params.filters;
    }
    this.makeRequest()
  }

  filterAdmission = event => {
    this.setState({[event.target.name]: event.target.value});
    delete this.state.params.min;
    delete this.state.params.max;
    var range = event.target.value !== -1 ? rangesAdmission[event.target.value] : {min: "", max: ""}
    var idx = this.state.filters.indexOf('admission_rate');
    var state_idx = this.state.filters.indexOf('state');
    var num_idx = idx

    if (state_idx !== -1 && num_idx > state_idx){
      num_idx = num_idx - 1
    }

    if (event.target.value === -1 && idx !== -1) {
      this.state.filters.splice(idx, 1);
      this.state.mins.splice(num_idx, 1);
      this.state.maxes.splice(num_idx, 1);
    } else {
      if (idx === -1) {
        this.state.filters.push('admission_rate')
        this.state.mins.push(range.min)
        this.state.maxes.push(range.max)
      } else {
        this.state.filters.splice(idx, 1);
        this.state.mins.splice(num_idx, 1);
        this.state.maxes.splice(num_idx, 1);
        this.state.filters.push('admission_rate')
        this.state.mins.push(range.min)
        this.state.maxes.push(range.max)
      }
    }
    if (this.state.filters.length > 0) {
      this.state.params.filters = this.state.filters.join(',');
      this.state.params.mins = this.state.mins.join(',');
      this.state.params.maxes = this.state.maxes.join(',');
    } else  {
      delete this.state.params.filters;
      delete this.state.params.mins;
      delete this.state.params.maxes;
    }
    this.makeRequest()
  }

  makeRequest() {
    axios.get(universitiesSearchUrl, {
      params: this.state.params
    }).then((response) => {
      this.setState({universities: response.data.data, snippets: response.data.snippets})
    }).catch((error) => this.setState({universities: [], error: "No results for search"}))
  }

  render() {
    if (this.state.loaded) {
    return (
      <div className="root">
        <Typography variant="h3" align="center" gutterBottom>
            Universities 
        </Typography>
        <div className={"search-filter"}>
          <Grid container spacing={8}>
            <Grid item xs={12}>
            <TextField
              style={{width: "100%"}}
              id="outlined-bare"
              placeholder="Search..."
              margin="normal"
              autoComplete="false"
              onChange={(e) => {this.setState({searchQuery: e.target.value})}}
              onKeyDown={(e) => {this.search(e, 0)}}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton className={'icon'} onClick={(e) => {this.search(e, 1)}}>
                    <Search />
                    </IconButton>
                  </InputAdornment>
                ),
                classes: {
                  root: 'cssLabel',
                },
              }}
            />
            </Grid>
            <Grid container item xs={12} spacing={8}>
            <Grid item xs={12} sm={6} md={3}>
            <FormControl style={{width: "100%"}}>
              <InputLabel htmlFor="sort-by">Sort by name</InputLabel>
              <Select
                value={this.state.sort_by}
                onChange={this.sortBy}
                inputProps={{
                  name: 'sort_by',
                  id: 'sort-by',
                }}
              >
                <MenuItem value={0}>Ascending</MenuItem>
                <MenuItem value={1}>Descending</MenuItem>
              </Select>
            </FormControl>
            </Grid>

              <Grid item xs={12} sm={6} md={3}>
              <FormControl style={{width: "100%"}}>
                <InputLabel htmlFor="sort-by">State</InputLabel>
                <Select
                  value={ this.state.state == -1 ? "" : this.state.state}
                  onChange={this.filterState}
                  inputProps={{
                    name: 'state',
                    id: 'state',
                  }}
                >
                  <MenuItem value={-1}><em>Any</em></MenuItem>
                  {states.map((state, idx) => 
                    <MenuItem value={states[idx]}>{state}</MenuItem>)}
                </Select>
              </FormControl>
              </Grid>

              <Grid item xs={12} sm={6} md={3}>
              <FormControl style={{width: "100%"}}>
                <InputLabel htmlFor="population">Population</InputLabel>
                <Select
                  value={ this.state.population == -1 ? "" : this.state.population}
                  onChange={this.filterPopulation}
                  inputProps={{
                    name: 'population',
                    id: 'population',
                  }}
                >
                  <MenuItem value={-1}><em>Any</em></MenuItem>
                  <MenuItem value={0}>&lt; 1K</MenuItem>
                  <MenuItem value={1}>1K - 10K</MenuItem>
                  <MenuItem value={2}>10K - 20K</MenuItem>
                  <MenuItem value={3}>20K - 30K</MenuItem>
                  <MenuItem value={4}>30K - 40K</MenuItem>
                  <MenuItem value={5}>&gt; 40K</MenuItem>
                </Select>
              </FormControl>
              </Grid>
              
              <Grid item xs={12} sm={6} md={3}>
              <FormControl style={{width: "100%"}}>
                <InputLabel htmlFor="admission-rate">Admission rate</InputLabel>
                <Select
                  value={ this.state.admission_rate == -1 ? "" : this.state.admission_rate}
                  onChange={this.filterAdmission}
                  inputProps={{
                    name: 'admission_rate',
                    id: 'admission-rate',
                  }}
                >
                  <MenuItem value={-1}><em>Any</em></MenuItem>
                  <MenuItem value={0}>&lt; 20%</MenuItem>
                  <MenuItem value={1}>20% - 40%</MenuItem>
                  <MenuItem value={2}>40% - 60%</MenuItem>
                  <MenuItem value={3}>60% - 80%</MenuItem>
                  <MenuItem value={4}>&gt; 80%</MenuItem>
                </Select>
              </FormControl>
              </Grid>
              </Grid>
          </Grid>
        </div>
        {this.state.universities.length > 0 ? 
        (<div>
          <Grid container spacing="24" justify="left"
            style={{
              marginLeft: 'auto',
              marginRight: '0',
              marginBottom: '1%',
              width: '100%',
            }}
            xs={10}>
            {this.state.universities.slice(this.state.offset, this.end()).map((university, idx) =>
              <Grid item>
                <Card className="my-card" name="universityCard">
                  <CardActionArea href={`/universities/${university.school_name}`}>
                    <CardMedia
                      className="testCardMedia"
                      image={university.images[0]}>
                    </CardMedia>
                    <CardContent>
                      <Typography noWrap gutterBottom variant="h6" component="h2">
                        {university.school_name}
                      </Typography>
                      <Typography gutterBottom component="p">
                        <ul>
                          <li>{university.city}, {university.state}</li>
                          <li>Admission rate: {university.admission_rate !== null ? (university.admission_rate * 100).toLocaleString(undefined, {maximumFractionDigits: 2}) + "%" : "Not available"}</li>
                          <li>Undergrad population: {university.undergrad_size.toLocaleString(undefined, {minimumIntegerDigits: 1})}</li>
                        </ul>
                      </Typography>
                      {this.state.snippets.length > 0 && 
                        <Highlighter
                        highlightClassName="highlight"
                        searchWords={[this.state.searchQuery]}
                        autoEscape={true}
                      textToHighlight={this.getColumnName(this.state.snippets[idx].column) + ":" + this.state.snippets[idx].snippet}/>}                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            )}
          </Grid>
          <Pagination
          style={{textAlign: "center", marginBottom: '1%'}}
          size="large"
          limit={9}
          offset={this.state.offset}
          total={this.state.universities.length}
          onClick={(e, offset) => this.handleClick(offset)}
          />
        </div>) : (<Typography style={{width: "100%", textAlign: "center"}}variant="h4">{this.state.error}</Typography>)}
      </div>
    );
  }
  else {
    return null
  }
  }
}

export default Universities
