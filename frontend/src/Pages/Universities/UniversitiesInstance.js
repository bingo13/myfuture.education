import React, { Component } from 'react';
import { Typography, CardMedia, CardContent, Card, Divider, CardActionArea } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Carousel from 'react-bootstrap/Carousel';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import axios from 'axios'
import purple from '@material-ui/core/colors/purple'
import blue from '@material-ui/core/colors/blue'
import pink from '@material-ui/core/colors/pink'
import red from '@material-ui/core/colors/red'
import teal from '@material-ui/core/colors/teal'
import {Pie} from 'react-chartjs-2';



const styles = theme => ({
  root: {
    width: '50%',
    margin: 'auto'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

const citiesUrl = 'https://api.myfuture.education/cities';
const majorsUrl = 'https://api.myfuture.education/majors';
const universitiesUrl = 'https://api.myfuture.education/universities';

class UniversitiesInstance extends Component {
  constructor(props) {
    super(props);
    this.state={
      loaded: false,
      university_data: null,
      majors: [],
      city: null
    }
  }

  componentDidMount() {
    var university = this.props.match.params.university
    axios.get(universitiesUrl, {
      params: {
        name: university,
      }
    }).then((response) => 
      {
      this.setState({university_data: response.data}, () => {
        this.state.university_data.top_majors.forEach((element) => {
          axios.get(majorsUrl, {
            params: {
              major: element.major,
              columns: 'major,images'
            }
          }).then((response) => {
            const updateList = this.state.majors.slice()
            updateList.push(response.data)
            this.setState({majors: updateList}, () => 
              axios.get(citiesUrl, {
                params: {
                  city: this.state.university_data.city,
                  state: this.state.university_data.state,
                  columns: 'name,state,pop_total,income,images'
                }
              }).then((response) => {
                this.setState({city: response.data}, () => 
                  this.setState({loaded: true})
                )
              })
            )
          })
        })
    })
  }).catch((error) =>
      console.log(error)
    )
    document.title = `${university} | My Future Education`
  }

  getEthnicityData() {
    var ethnicityPopData = {
      labels: [
        'Asian',
        'Black',
        'White',
        'Hispanic',
        'Other',
      ],
      datasets: [{
        data: [
          this.state.university_data.race_ethnicity_asian + this.state.university_data.race_ethnicity_aian,
          this.state.university_data.race_ethnicity_black,
          this.state.university_data.race_ethnicity_white,
          this.state.university_data.race_ethnicity_hispanic,
          this.state.university_data.race_ethnicity_nhpi +
          this.state.university_data.race_ethnicity_non_resident_alien +
          this.state.university_data.race_ethnicity_unknown + 
          this.state.university_data.race_ethnicity_two_or_more,
        ],
        backgroundColor: [
          purple[400],
          blue[400],
          red[400],
          pink[400],
          teal[400],
        ],
        hoverBackgroundColor: [
          purple[400],
          blue[400],
          red[400],
          pink[400],
          teal[400],
        ]
      }],
    }
    return ethnicityPopData;
  }

  render() {
    const { classes } = this.props;
    if (this.state.loaded) {
    return(
      <div className={classes.root}>
        <Typography variant="h3" align="center" gutterBottom>
          {this.state.university_data.school_name}
        </Typography>

        <Carousel className="Carousel-container">
        {this.state.university_data.images.map((url) =>
          <Carousel.Item>
            <div style={{width: "100%", paddingBottom: "56.25%", position: "relative", marginTop: "2%", marginBottom: "2%"}}>
              <img
                className="Carousel-content"
                src={url}
                alt="Campus"
              />
            </div>
          </Carousel.Item>
        )}
        </Carousel>
        <div className="Charts">
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
              <Typography className={classes.heading}>
                Info
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div style={{width: '100%'}}>
                <Typography variant="subtitle1" align="left">
                  School website: <a href={'https://' + this.state.university_data.school_url} target='_blank' rel="noopener noreferrer">{this.state.university_data.school_url} </a>
                </Typography>
                <Typography variant="subtitle1" align="left">
                  City: {this.state.university_data.city}, {this.state.university_data.state}
                </Typography>
                <Typography variant="subtitle1" align="left">
                  Undergrad population: {this.state.university_data.undergrad_size.toLocaleString(undefined, {minimumIntegerDigits: 1})}
                </Typography>
                <Typography variant="subtitle1" align="left">
                  Type: { this.state.university_data.private_tuition == null ? 'Public' : 'Private' }
                </Typography>
                <Typography variant="subtitle1" align="left">
                  Admission rate: {this.state.university_data.admission_rate !== null ? (this.state.university_data.admission_rate * 100).toLocaleString(undefined, {maximumFractionDigits: 2}) + "%" : "Not available"}
                </Typography>
                <Typography variant="subtitle1" align="left">
                  Graduation rate: {(this.state.university_data.graduation_rate * 100).toLocaleString(undefined, {maximumFractionDigits: 2})}%
                </Typography>
                <Typography variant="subtitle1" align="left">
                  Median income after graduation: ${this.state.university_data.median_income.toLocaleString(undefined, {maximumFractionDigits: 2})}
                </Typography>
              </div>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
              <Typography className={classes.heading}>
                Cost
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div style={{width: '100%'}}>
                <Typography variant="subtitle1" align="left">
                In-state tuition: ${this.state.university_data.in_state_tuition.toLocaleString(undefined, {minimumFractionDigits: 2})}
                </Typography>
                <Typography variant="subtitle1" align="left">
                  Out-of-state tuition: ${this.state.university_data.out_of_state_tuition.toLocaleString(undefined, {minimumFractionDigits: 2})}
                </Typography>
              </div>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
            <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
              <Typography className={classes.heading}>
                SAT/ACT Scores
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <div style={{width: '100%'}}>
                <Typography variant="subtitle1" align="left">
                  SAT average: {this.state.university_data.sat_average == null ? "Not available" : this.state.university_data.sat_average}
                </Typography>
                <Typography variant="subtitle1" align="left">
                  ACT average: {this.state.university_data.act_midpoint == null ? "Not available" : this.state.university_data.act_midpoint }
                </Typography>
              </div>
            </ExpansionPanelDetails>
          </ExpansionPanel>
          <ExpansionPanel>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
                <Typography className={classes.heading}>
                  Population distribution
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <div style={{width: '100%'}}>
                  <div style={{paddingTop: "20px", paddingBottom: "30px"}}>
                    <Typography variant="h6" align="center">
                      Percent distribution by ethnicity
                    </Typography>
                    <Pie 
                    data={ this.getEthnicityData() } 
                    options={{
                      tooltips: {
                        callbacks: {
                          label: function(toolTipItem, data) {
                            return (data.datasets[toolTipItem.datasetIndex].data[toolTipItem.index] * 100).toLocaleString(undefined, {maximumFractionDigits: 2}) + "%";
                          }
                        }

                      }
                    }} />
                  </div>
                </div>
              </ExpansionPanelDetails>
            </ExpansionPanel>
        </div>
        <Divider/>
        <Typography style={{marginTop: '2%', marginBottom: '2%'}} variant="h5">
          Location
        </Typography>
        <CardActionArea href={`/cities/${this.state.city.name}_${this.state.city.state}`}>
          <Card className="Card">
            <CardMedia
              image={this.state.city.images[0]}
              className="Card-Media"
            />
            <div className="Card-Details">
              <CardContent>
                <Typography variant="h5" noWrap>
                  {this.state.city.name}, {this.state.city.state}
                </Typography>
                <Typography variant="subtitle1" noWrap>
                  Population: {this.state.city.pop_total.toLocaleString(undefined, {maximumFractionDigits: 0})}
                </Typography>
                <Typography variant="subtitle1" noWrap>
                  Median income: ${this.state.city.income.toLocaleString(undefined, {minimumFractionDigits: 2})}
                </Typography>
              </CardContent>
            </div>
          </Card>
        </CardActionArea>
        <Divider/>
        <Typography style={{marginTop: '2%', marginBottom: '2%'}} variant="h5">
          Top Majors at {this.state.university_data.school_name}
        </Typography>
        {this.state.majors.map((major) =>
          <CardActionArea href={`/majors/${major.major}`}>
            <Card className="Card">
              <CardMedia
                image={major.images[0]}
                className="Card-Media"
              />
              <div className="Card-Details">
                <CardContent>
                  <Typography variant="h5" noWrap>
                    {major.major.replace(/\w\S*/g, function(txt) {
                      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                      })
                    }
                  </Typography>
                  <Typography gutterBottom variant="subtitle1" noWrap>Field of study: {major.area_of_study.replace('_', ' ').replace(/\w\S*/g, function(txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                  })}</Typography>
                  <Typography gutterBottom variant="subtitle1" noWrap>Starting salary: ${major.starting_salary.toLocaleString(undefined, {maximumFractionDigits: 0})}</Typography>
                </CardContent>
              </div>
            </Card>
          </CardActionArea>
        )}
      </div>
    );
    }
    else {
      return null
    }
  }
}

UniversitiesInstance.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UniversitiesInstance)
