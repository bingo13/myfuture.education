import React, { Component } from 'react';
import './IncomeGraph.css';
import { scaleLinear, scaleBand } from 'd3-scale';
import { max} from 'd3-array';
import { select } from 'd3-selection';
import {axisBottom, axisLeft} from 'd3-axis';
import {format} from 'd3-format';
import * as d3 from 'd3';
import axios from 'axios';


const citiesUrl = 'https://api.myfuture.education/cities'
const races = ["2 Or More", "Asian", "Black", "Hawaiian", "Hispanic", "Native", "White"]

class IncomeGraph extends Component{
  constructor(props){
    super(props);
    this.state = {
        incomes: [0],
        params: {},
        loaded: false
    }
  }

  componentDidMount(){
    this.makeRequest()
    if(this.state.loaded){
        this.createBarChart()
    }
  }

  componentDidUpdate(){
    if(this.state.loaded){
        this.createBarChart()
    }
  }

  makeRequest(){
      var columnStart = 'income'
      var myParams = [columnStart+"_2ormore"]
      for(let i = 1;i<races.length;i++){
          myParams.push(columnStart+"_"+races[i].toLowerCase())
          this.state.incomes.push(0)
      }
      this.state.params.columns = myParams.join(",")
      axios.get(citiesUrl, {
        params: this.state.params
      })
      .then((response) => {
        response.data.data.forEach(element => {
           for (let i = 0; i<myParams.length;i++){
               this.state.incomes[i] = this.state.incomes[i] + element[myParams[i]]
           }
        });
        for (let i = 0; i<this.state.incomes.length;i++){
            this.state.incomes[i] = (this.state.incomes[i] / response.data.data.length)
        }
        this.setState({loaded:true})
      })
  }

  createBarChart(){
    const margin = {
        top:30,
        bottom: 30,
        left: 50,
        right: 50
    }
    const node = this.node
    const dataMax = max(this.state.incomes)
    var mid = (margin.left + margin.right) + (this.props.size[0] - margin.right)
    mid = mid / 2
    const xScale = scaleBand()
        .domain(races)
        .range([margin.left + margin.right, this.props.size[0] - margin.right])
        .padding(.1)
    const yScale = scaleLinear()
      .domain([0, dataMax]).nice()
      .range([this.props.size[1] - margin.bottom - margin.top, margin.top])
    const yAxis = axisLeft()
        .scale(yScale)
        .tickSize(10)
        .tickFormat(d=>format('$,')(d))
    const xAxis = axisBottom()
        .scale(xScale)
        .tickSize(10)

    select(node)
      .selectAll('rect')
      .data(this.state.incomes)
      .enter()
      .append('rect')
        .attr("x", (d,i) => xScale(races[i]))
        .attr("y", (d,i) => yScale(d))
        .attr('height', (d,i)=> yScale(0) - yScale(d))
        .attr('width', xScale.bandwidth())
        .attr('fill', 'blue')
        .on('mouseover', (d,i)=>mouseOver(d,i))
        .on('mouseout', (d,i)=>mouseOut(i))

      // .append("text")
      //   .classed('bar-label', true)
      //   .attr("id", (d,i)=>"r"+i)
      //   .attr('x', (d,i)=>xScale(races[i])+ xScale.bandwidth()/2)
      //   .attr('dx', 0)
      //   .attr('y', (d,i)=>yScale(d))
      //   .attr('dy', -6)
        // .text(d=>format('$,.2f')(d))

    select(node)
      .append('text')
      .attr("x", mid)             
      .attr("y", margin.top / 2)
      .style('font-size', '20px')
      .style('text-anchor', 'middle')
      .text("Income Across Races in College Cities")
    
    select(node)
        .selectAll("bar-label")
        .data(this.state.incomes)
        .enter()
        .append("text")
        .classed('bar-label', true)
        .attr("id", (d,i)=>"r"+i)
        .attr('x', (d,i)=>xScale(races[i])+ xScale.bandwidth()/2)
        .attr('dx', 0)
        .attr('y', (d,i)=>yScale(d))
        .attr('dy', -6)
        // .text(d=>format('$,.2f')(d))

    select(node)
        .append('g')
        .attr('id', 'yAxis')
        .attr('transform', 'translate('+(margin.left + margin.right)+',0)')
        .call(yAxis)
        .append('text')
        .attr('x', 0)
        .attr('y', 0)
        .attr('transform', 'translate(-'+(margin.left + 5)+`, ${this.props.size[1]/2}) rotate(-90)`)
        .attr('fill', '#000')
        .style('font-size', '20px')
        .style('text-anchor', 'middle')
        .text('Average Income Across The Country')

    select(node)
        .append('g')
        .attr('id', 'xAxis')
        .attr('transform', 'translate(0,'+(this.props.size[1]-margin.bottom-margin.top)+')')
        .call(xAxis)
        .append('text')
        .attr('x', this.props.size[0]/2)
        .attr('y', 40)
        .attr('fill', '#000')
        .style('font-size', '20px')
        .style('text-anchor', 'middle')
        .text('Race')

    function mouseOver(d,i){
      d3.select("#r"+i).transition().duration(200).style("opacity", .9);
      d3.select("#r"+i).text(format('$,.2f')(d))
    }

    function mouseOut(i){
      d3.select("#r"+i).transition().duration(500).style("opacity", 0);
    }
  }

  render(){
  	return(
      <svg ref = {node => this.node = node} width={this.props.size[0]} height={this.props.size[1]}>
      </svg>
  	);
  }
}

export default IncomeGraph;
