import React, { Component } from 'react';
import {format} from 'd3-format';
import { select } from 'd3-selection';
import {arc, pie} from 'd3-shape';
import {interpolateRainbow} from 'd3-scale-chromatic'
import axios from 'axios';
import * as d3 from 'd3'

const eventsURL = 'https://api.volunteer4.me/eventTypes?all=True'

class PieChart extends Component{
  constructor(props){
    super(props);
    this.state = {
      totalEvents: 0,
      events: [],
      loaded: false
    }
  }

  componentDidMount(){
    this.makeRequest()
    if(this.state.loaded){
      this.createPieChart()
    }
  }

  componentDidUpdate(){
    if(this.state.loaded){
      this.createPieChart()
    }
  }

  makeRequest(){
    axios.get(eventsURL)
    .then(res=>{
      res.data.forEach(element => {
        let count = element['num_events']
        this.setState({totalEvents:this.state.totalEvents + count})
      });

      res.data.forEach(element => {
        let name = element['event_type_name']
        let count = element['num_events']
        let percent = count / this.state.totalEvents
        this.state.events.push({'name':name, 'count':count, 'percent':percent})
      });
      this.setState({loaded:true})
    })
  }



createPieChart(){
    function tooltipHtml(name, count, percent){
      var htmlTable = "<h4>"+name+"</h4><table>";
      htmlTable += "<tr><td>Count: <td>" +(count) + "</td></td></tr>"
      htmlTable += "<tr><td>Percent: <td>" +(format('.2%')(percent)) + "</td></td></tr></table>"
      return htmlTable
    }

    function mouseOver(d){
      d3.select("#pietip").transition().duration(200).style("opacity", .9);
      d3.select("#pietip").html(tooltipHtml(d.data.name, d.data.count, d.data.percent))
    }

    function mouseOut(){
      d3.select("#pietip").transition().duration(500).style("opacity", 0);
    }
    const node = this.node
    const arcs = pie().value(d=>d.count)(this.state.events)
    const radius = Math.min(this.props.size[0]/2, this.props.size[1]/2)
    const singleArc = arc().innerRadius(0).outerRadius(radius-10)
    select(node)
      .append('g')
      .attr('id','piechart')
      .attr('transform', 'translate('+this.props.size[0]/2+','+this.props.size[1]/2+')')
        .selectAll('path')
        .data(arcs)
        .enter()
          .append('path')
          .attr('id', d=>d.data.name)
          .attr("fill", (d,i) => interpolateRainbow(i / arcs.length))
          .attr('stroke', 'black')
          .attr('d', singleArc)
          .on("mouseover", d=>mouseOver(d))
          .on("mouseout", d=>mouseOut())
  }

  render(){
  	return(
      <div>
        <div width="100" height="100" id="pietip"></div>
        <svg ref = {node => this.node = node} width={this.props.size[0]} height={this.props.size[1]}>
        </svg>
    </div>
  	);
  }
}

export default PieChart;