import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTheme } from '@material-ui/core/styles';
import { Typography, Divider } from '@material-ui/core';
import IncomeGraph from './IncomeGraph';
import Choropleth from './Choropleth';
import PieChart from './PieChart';
import './Visualizations.css';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import MajorDensityGraph from './MajorDensityGraph';
import './Visualizations.css';

class Visualizations extends Component{
  constructor(props){
    super();
    this.state = {
      value: 0
    }
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  render() {
    const { theme } = this.props;
    return(
      <div className="root">
        <AppBar position="relative" color="primary" style={{marginTop: '-40px', marginBottom: "2%", boxShadow: '0 6px 5px -3px gray'}}>
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            indicatorColor="secondary"
            textColor="inherit"
            className="tabs"
            variant="fullWidth"
          >
            <Tab classes={{root: "tabs"}} label="My Future Education" />
            <Tab classes={{root: "tabs"}} label="Volunteer 4 Me" />
          </Tabs>
        </AppBar>
        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={this.state.value}
          onChangeIndex={this.handleChangeIndex}
          animateHeight
        >
          <div>
            <div align = 'center'>
              <IncomeGraph size={[window.screen.width / 5 * 4, 500]}/>
            </div>
            <Divider/>
            <Typography variant="h4" align="center" gutterBottom>
                Number of Universities per State
            </Typography>
            <Typography variant="subtitle1" align="center" color="default">
              This visualizations displays the number of universities per state
              selected in our database.
            </Typography>
            <div align="center">
              <Choropleth size={[window.screen.width / 5 * 4, 500]}/>
            </div>
            <Divider/>
            <div className='caption'>
            <Typography variant="h4" align="center" gutterBottom>
              Major Density per University
            </Typography>
            <Typography variant="subtitle1" align="center" color="default">
              A zoomable circle graph showing the density of majors for each university in our database, and their relative sizes.
              The size of each university circle is roughly dependent on its total undergraduate population;
              the relative size of the majors' circle within are accurate to their proportions in our data.
            </Typography>
            <br />
          </div>
          <div align="center">
            <MajorDensityGraph size={[window.screen.width / 5 * 4, window.screen.width / 5 * 4]}/>
          </div>
          <br/>
          </div>
          <div>
            <div align = 'center'>
              <PieChart size={[window.screen.width / 5 *4,500]}/>
            </div>
          </div>
        </SwipeableViews>          
      </div>
    );
  }
}

Visualizations.propTypes = {
  theme: PropTypes.object.isRequired,
};

export default withTheme()(Visualizations);
