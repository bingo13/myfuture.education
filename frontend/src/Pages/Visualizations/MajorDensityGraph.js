import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './IncomeGraph.css';
import * as d3 from 'd3';
import axios from 'axios';

const universitiesURL = 'https://api.myfuture.education/universities'
const columns = ['school_name','undergrad_size','percent_of_degrees']

class MajorDensityGraph extends Component {
    constructor(props) {
        super(props);
        this.state = {
            universities: [0],
            params: {},
            loaded: false,
            width: props.size[0],
            height: props.size[1]
        }
    }

    componentDidMount(){
        this.makeRequest()
        if (this.state.loaded){
            this.createDensityGraph()
        }
    }

    getColumnName(column) {
        column = column.replace(new RegExp('_', 'g'), ' ');
        return column.charAt(0).toUpperCase() + column.substr(1).toLowerCase()
    }

    componentDidUpdate(){
        if (this.state.loaded){
            this.createDensityGraph()
        }
    }

    makeRequest() {
        this.state.params.columns = columns.join(",")
        this.state.params.universities = []
        axios.get(universitiesURL, {
            params: this.state.params
        }).then((response) => {
            response.data.data.forEach(element => {
                let quantifiedDegrees = []
                for(let index in element.percent_of_degrees){
                    let size = element.percent_of_degrees[index] * element.undergrad_size
                    if (size !== 0){

                        quantifiedDegrees.push({'name': this.getColumnName(index), 'value':size})
                    }
                }
                this.state.params.universities.push({
                    'name': element.school_name,  
                    'children': quantifiedDegrees})
            })
            this.setState({loaded:true})
        })
    }

    createDensityGraph() {
        const width = this.state.width;
        const height = this.state.height;
        console.log("width: " + width + " height: " + height);
        let data = {'name': 'temp', 'children': this.state.params.universities};
        let pack = data => d3.pack()
            .size([width, height])
            .padding(3)
            (d3.hierarchy(data)
            .sum(d => d.value)
            .sort((a, b) => b.value - a.value))
        let color = d3.scaleLinear()
            .domain([0, 5])
            .range(["hsl(152,80%,80%)", "hsl(228,30%,40%)"])
            .interpolate(d3.interpolateHcl)
        const root = pack(data);
        let focus = root;
        let view;

        const svg = d3.select('#circles')
            .attr("viewBox", `-${width / 2} -${height / 2} ${width} ${height}`)
            .style("display", "block")
            .style("margin", "0 -14px")
            .style("height", "auto")
            .style("background", color(0))
            .style("cursor", "pointer")
            .on("click", () => zoom(root));
        console.log("svg made");
        const node = svg.append("g")
            .selectAll("circle")
            .data(root.descendants().slice(1))
            .join("circle")
            .attr('id', d=>d.name)
            .attr("fill", d => d.children ? color(d.depth) : "white")
            .attr("pointer-events", d => !d.children ? "none" : null)
            .on("mouseover", d => { d3.select('#'+d.name).attr("stroke", "#000"); })
            .on("mouseout", d => { d3.select('#'+d.name).attr("stroke", null); })
            .on("click", d => focus !== d && (zoom(d), d3.event.stopPropagation()));

        const label = svg.append("g")
            .style("font", "10px sans-serif")
            .attr("pointer-events", "none")
            .attr("text-anchor", "middle")
            .selectAll("text")
            .data(root.descendants())
            .join("text")
            .style("fill-opacity", d => d.parent === root ? 1 : 0)
            .style("display", d => d.parent === root ? "inline" : "none")
            .text(d => d.data.name);

        zoomTo([root.x, root.y, root.r * 2]);

        function zoomTo(v) {
            const k = width / v[2];

            view = v;

            label.attr("transform", d => `translate(${(d.x - v[0]) * k},${(d.y - v[1]) * k})`);
            node.attr("transform", d => `translate(${(d.x - v[0]) * k},${(d.y - v[1]) * k})`);
            node.attr("r", d => d.r * k);
        }

        function zoom(d) {
            const focus0 = focus;

            focus = d;

            const transition = svg.transition()
                .duration(d3.event.altKey ? 7500 : 750)
                .tween("zoom", d => {
                    const i = d3.interpolateZoom(view, [focus.x, focus.y, focus.r * 2]);
                    return t => zoomTo(i(t));
                });

            label
                .filter(function (d) { return d.parent === focus || this.style.display === "inline"; })
                .transition(transition)
                .style("fill-opacity", d => d.parent === focus ? 1 : 0)
                .on("start", function (d) { if (d.parent === focus) this.style.display = "inline"; })
                .on("end", function (d) { if (d.parent !== focus) this.style.display = "none"; });
        }
    }

    render() {

        return (
            <svg id='circles'width = {this.props.size[0]} height = {this.props.size[1]}></svg>
        )
    };
}

export default MajorDensityGraph;