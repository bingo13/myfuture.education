import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import Ali from './../../Images/Ali_Naqvi.jpeg';
import Chris from './../../Images/Christian_Gaitan.jpg';
import Daniel from './../../Images/Daniel_Andrade.jpg';
import Paulina from './../../Images/Paulina_Kuo.jpg';
import Pooja from './../../Images/Pooja.jpg';
import SlackLogo from './../../Images/Slack_Mark.svg';
import AWSLogo from './../../Images/aws.svg';
import ReactLogo from './../../Images/react.svg';
import PostmanLogo from './../../Images/postman.svg';
import DockerLogo from './../../Images/docker.png';
import GitLabLogo from './../../Images/gitlab_logo.png';
import GoogleLogo from './../../Images/google.svg';
import DOELogo from './../../Images/doe.svg';
import WikipediaLogo from './../../Images/wikipedia.svg';
import CensusLogo from './../../Images/census.svg';
import DataUSALogo from './../../Images/datausa.png';
import { withStyles } from '@material-ui/core/styles';
import { CardMedia, CardContent, Typography, CardActions, IconButton, Collapse, Link, CardActionArea, Paper } from '@material-ui/core';

const styles = theme => ({
  grid:{
    width: '100%',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  card: {
    width: 275,
    height: '100%'
  },
  media: {
    objectFit: 'cover'
  },
  action:{
    display: 'flex'
  },link: {
    margin: theme.spacing.unit,
  },
  expand: {
    outline: 'none !important',
    transform: 'rotate(0deg)',
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  aboutSubtitle: {
    width: '80%',
    minWidth: '400px',
    marginLeft: 'auto',
    marginRight: 'auto',
    textAlign: 'justify'
  },
  repoPaper: {
    width: '20%',
    minWidth: '175px',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  root: {
    width: '60%',
    marginLeft: 'auto',
    marginRight: 'auto'
  }
});

class About extends Component{
  constructor(){
    super();
    this.state = {
      error: null,
      call1: false,
      call2: false,
      profiles: {
        'AliNaqvi97':{
          expand: true,
          name:"Ali Naqvi",
          role: 'Frontend Engineer',
          image:Ali,
          bio:"4th year student at UTCS who listens to a lot of music, likes to hangout with friends, play video games, and enjoys helping others.",
          gitlabID:3516493,
          commits:0,
          issues:0,
          tests:0
        },
        'Christian Gaitan':{
          expand: true,
          name:"Christian Gaitan",
          role: 'Backend Engineer',
          image:Chris,
          bio:"Junior computer science student at UT who loves cats, and playing video games.",
          gitlabID:3486311,
          commits:0,
          issues:0,
          tests:27
        },
        'Daniel Andrade':{
          expand: true,
          name:"Daniel Andrade",
          role: 'Frontend Engineer',
          image:Daniel,
          bio:"Senior computer science student at UT who loves to play video games and work on his project car.",
          gitlabID:3543327,
          commits:0,
          issues:0,
          tests:19
        },
        'Paulina Kuo':{
          expand: true,
          name:"Paulina Kuo",
          role: 'Frontend Engineer',
          image:Paulina,
          bio:"Senior at UT studying computer science. She loves yoga, giving campus tours, and enjoying her last months on campus.",
          gitlabID:3543423,
          commits:0,
          issues:0,
          tests:10
        },
        'Pooja Maiti':{
          expand: true,
          name:"Pooja Maiti",
          role: 'Backend Engineer',
          image:Pooja,
          bio:"Junior computer science student at UT who loves to listen to Bollywood music, drink coffee, and to sleep",
          gitlabID:3506096,
          commits:0,
          issues:0,
          tests:27
        }
      },
      totalCommits:0,
      totalIssues:0,
      totalTests:50
    };
  }

  getTotalTests(){
    let sum = 0;
    Object.keys(this.state.profiles).map(name => {
      let member = this.state.profiles[name];
      sum += member.tests;
    });
    // this.state.profiles.forEach((profile) => {
    //   sum += profile.tests;
    // });
    return sum;
  }

  async componentDidMount(){
    document.title = 'About | My Future Education';
    let baseURL = "https://gitlab.com/api/v4/projects/11008137";
    let commitsExt = "/repository/commits?all=true&per_page=100&page=";
    let issuesExt = "/issues?author_id=";

    /* Get all the commits to the project and  get counts per member*/
    let page = 0
    let finished = false
    while(!finished){
      ++page
      await fetch(baseURL+commitsExt+page)
      .then(res => res.json())
      .then(result => {
          result.forEach(element => {
            let name = element["author_name"];
            if (name.toLowerCase().includes("gaitan")){
              name = "Christian Gaitan"
            } else if (name.includes("Paulina")){
              name = "Paulina Kuo"
            } else if (name.includes("Daniel")){
              name = "Daniel Andrade"
            } else if (name.includes("Ali")){
              name = "AliNaqvi97"
            }
            let member = this.state.profiles[name];
            member.commits +=1;
            let prop = this.state.profiles;
            prop[name] = member;
            this.setState({profiles:prop})
          });
          if (result.length < 100){
            finished = true
          }
          this.setState({
            totalCommits:this.state.totalCommits+result.length,
            call1: true
          });
      }, error => {
        this.setState({
          call1: true,
          error
        })
      });
    }


    /* Loop through members and count their issues. */
    let dict = this.state.profiles;
    let names = Object.keys(dict);
    let count = 0;
    names.forEach(name => {
      fetch(baseURL+issuesExt+dict[name].gitlabID)
      .then(res => res.json())
      .then(result => {
        let issueLength = result.length;
        let member = this.state.profiles[name];
        member.issues = issueLength;
        let prop = this.state.profiles;
        prop[name] = member;
        this.setState({
          profiles:prop,
          totalIssues: this.state.totalIssues + issueLength
        })
        count++;
        if (count === names.length){
          this.setState({call2:true})
        }
      }, error => {
        this.setState({
          call2: true,
          error
        })
      })
    });
  }

  handleClick(name){
    let member = this.state.profiles[name];
    member.expand = !member.expand;
    let prop = this.state.profiles;
    prop[name] = member;
    this.setState({profiles:prop})
  }

  render(){
    if (this.state.error) {
      return <div>Error: {this.state.error.message}</div>;
    } else{
      const { classes } = this.props;

      /* Loop through members and create a card for them */
      let dict = this.state.profiles;
      let names = Object.keys(dict);
      let cards = names.map(name => {
        let member = dict[name];
        return (
          <Grid item key={name}>
            <Card className={classes.card}>
              <CardMedia
                component="img"
                className={classes.media}
                height="225"
                image={member.image}>
              </CardMedia>

              <CardContent>
                <Typography variant="h5" gutterBottom>
                  {member.name}
                </Typography>
                <Typography>
                  {member.role}
                </Typography>
              </CardContent>

              <Collapse in={this.state.profiles[name].expand} timeout="auto" unmountOnExit>
                <CardContent>
                  <Typography gutterBottom>
                    {member.bio}
                  </Typography>
                  <Typography gutterBottom>
                    Commits: {member.commits}
                  </Typography>
                  <Typography gutterBottom>
                    Issues: {member.issues}
                  </Typography>
                  <Typography gutterBottom>
                    Unit Tests: {member.tests}
                  </Typography>
                </CardContent>
              </Collapse>
            </Card>
          </Grid>
        )
      })
      return (
        <div className={classes.root}>
          <Typography variant="h3" align="center" gutterBottom>
            Our Purpose
          </Typography>

          <Typography variant="subtitle1" align="left" gutterBottom className={classes.aboutSubtitle}>
            Our website helps prospective university students find universities that match their
            priorities. This site offers information on cities with colleges, acceptance
            requirements and rates, and information on fields of study.
          </Typography>
          
          <Typography variant="h3" gutterBottom/>

          
          <Typography variant="h3" align="center" gutterBottom>
            Data Integration
          </Typography>
          <Typography variant="subtitle1" align="left" gutterBottom className={classes.aboutSubtitle}>
            As a result of combining the data from our sources, we have discovered that some
            schools offer a wide range of degrees and some focus on a small set of fields of
            studies.
          </Typography>

          <Typography variant="h3" gutterBottom/>

          <Typography variant="h3" align="center" gutterBottom>
            The Team
          </Typography>
          <Grid container
            className={classes.grid}
            spacing={24}
            justify="center">
            {cards}
          </Grid>
          <Typography variant="h3" gutterBottom/>

          <Paper elevation={1} className={classes.repoPaper}>
            <Typography variant="subtitle1" align="center" gutterBottom>
              Total Commits: {this.state.totalCommits}
            </Typography>
            <Typography variant="subtitle1" align="center" gutterBottom>
              Total Issues: {this.state.totalIssues}
            </Typography>
            <Typography variant="subtitle1" align="center" gutterBottom>
              Total Tests: {this.getTotalTests()}
            </Typography>
          </Paper>
          
          <Typography variant="h3" gutterBottom/>

          <Typography variant="h3" align="center" gutterBottom>
            Data Sources
          </Typography>
          <Grid container spacing="24" justify="center"
            style={{
              marginBottom: '2%',
              width: '100%',
              }}>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href="https://collegescorecard.ed.gov/" target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={DOELogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      College ScoreCard
                    </Typography>
                    <Typography variant="subtitle1" align="left" gutterBottom>
                      Used to get information on universities in the US. College Scorecard provides a RESTful
                      API that returns a JSON reponse, which we parsed to retrieve the information.                     
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href="https://www.wikipedia.org/" target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={WikipediaLogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      Wikipedia
                    </Typography>
                    <Typography variant="subtitle1" align="left" gutterBottom>
                      Used to get information on college majors.The Wikipedia's RESTful API returns an XML page,
                      so an HTML parser script was written in order to obtain information that was relevant.                    
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href="https://www.census.gov/" target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={CensusLogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      Census Bureau
                    </Typography>
                    <Typography variant="subtitle1" align="left" gutterBottom>
                      Used primarily for population data for cities in the US. The provided RESTful
                      API returns a JSON response, which we parsed to retrieve the information.
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href="https://developers.google.com/custom-search/" target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={GoogleLogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      Google Custom Search API
                    </Typography>
                    <Typography variant="subtitle1" align="left" gutterBottom>
                      Used to supply images for our website. The provided RESTful
                      API  returns a JSON reponse, which we parsed to retrieve links to images.
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href="https://datausa.io/" target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={DataUSALogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      DataUSA
                    </Typography>
                    <Typography variant="subtitle1" align="left" gutterBottom>
                      Used primarily for financial information for cities in the US. The provided RESTful
                      API returns a JSON response, which we parsed to retrieve the information.
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          </Grid>
          <Typography variant="h3" gutterBottom/>

          <Typography variant="h3" align="center" gutterBottom>
            Tools
          </Typography>
          <Grid container spacing="24" justify="center"
            style={{
              marginBottom: '2%',
              width: '100%',
              }}>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href="https://aws.amazon.com/" target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={AWSLogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      AWS S3/CloudFront
                    </Typography>
                    <Typography variant="subtitle1" align="center" gutterBottom>
                      For hosting and distributing our website.
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href="https://slack.com/" target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={SlackLogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      Slack
                    </Typography>
                    <Typography variant="subtitle1" align="center" gutterBottom>
                      For communication within the team.
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href='https://reactjs.org/' target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={ReactLogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      React
                    </Typography>
                    <Typography variant="subtitle1" align="center" gutterBottom>
                      For designing the website.
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href="https://www.getpostman.com/" target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={PostmanLogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      Postman
                    </Typography>
                    <Typography variant="subtitle1" align="center" gutterBottom>
                      For designing our RESTful API.
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href="https://www.docker.com/" target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={DockerLogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      Docker
                    </Typography>
                    <Typography variant="subtitle1" align="center" gutterBottom>
                      To ensure similar environment during development .
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href="https://about.gitlab.com/" target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={GitLabLogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      GitLab
                    </Typography>
                    <Typography variant="subtitle1" align="center" gutterBottom>
                      For code storage and easier collaboration.
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
            <Grid item>
              <Card className={classes.card}>
                <CardActionArea href="https://domains.google/" target='_blank' rel='noopener noreferrer'>
                  <CardMedia
                    component="img"
                    className={classes.media}
                    height="225"
                    image={GoogleLogo}>
                  </CardMedia>
                  <CardContent>
                    <Typography variant="h5" align="center" gutterBottom>
                      Google Domains
                    </Typography>
                    <Typography variant="subtitle1" align="center" gutterBottom>
                      For a website domain and certificate management.
                    </Typography>
                  </CardContent>
                </CardActionArea>
              </Card>
            </Grid>
          </Grid>
          
          
          <Typography variant="h3" gutterBottom/>

          <Typography variant="h3" align="center" gutterBottom>
            Links
          </Typography>
          <Typography variant="subtitle1" align="center" gutterBottom>
            <Link href = "https://gitlab.com/bingo13/myfuture.education" target='_blank' rel="noopener noreferrer" id='gitlablinkAboutPage'>
              GitLab Repository
            </Link>
          </Typography>
          <Typography variant="subtitle1" align="center" gutterBottom>
            <Link href = "https://documenter.getpostman.com/view/6755621/S11KQJok" target='_blank' rel="noopener noreferrer" id='apidoclinkAboutPage'>
              API Documentation
            </Link>
            <br></br>
            <br></br>
            <br></br>
          </Typography>
        </div>
      );
    }
  }
}

export default withStyles(styles)(About);
