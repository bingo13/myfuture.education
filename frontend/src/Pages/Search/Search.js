import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withTheme } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import { CardActionArea, CardMedia, Typography, CardContent, Grid, Divider } from '@material-ui/core';
import './Search.css';
import Pagination from 'material-ui-flat-pagination';
import axios from 'axios';
import Highlighter from "react-highlight-words";
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';



const citiesSearchUrl = 'https://api.myfuture.education/search/cities';
const universitiesSearchUrl = 'https://api.myfuture.education/search/universities';
const majorsSearchUrl = 'https://api.myfuture.education/search/majors';

class Search extends Component{
  constructor(props) {
    super(props)
    this.state = {
      offsetCities: 0,
      offsetMajors: 0,
      offsetUniversities: 0,
      cities: null,
      majors: null,
      universities: null,
      snippetsMajors: [],
      snippetsCities: [],
      snippetsUniversities: [],
      majorsLoaded: false,
      citiesLoaded: false,
      universitiesLoaded: false,
      errorMajors: "",
      errorCities: "",
      errorUniversities: "",
      query: '',
      value: null
    }
  }
  componentDidMount() {
    this.setState({query: this.props.match.params.query}, ()  => this.search())
    document.title = 'Search | My Future Education';
  }
  
  handleClick(offset, type) {
    this.setState({[type]: offset})
  }

  end(type) {
    switch(type) {
      case 'universities':
      return this.state.offsetUniversities + 9 >= this.state.universities.length ? this.state.universities.length : this.state.offsetUniversities + 9;
      
      case 'cities':
        return this.state.offsetCities + 9 >= this.state.cities.length ? this.state.cities.length : this.state.offsetCities + 9;
      
      case 'majors':
        return this.state.offsetMajors + 9 >= this.state.majors.length ? this.state.majors.length : this.state.offsetMajors + 9;
    }
  }

  getTopUniversity(major) {
    var univ = ''
    var max = 0
    major.universities.forEach((element) => {
      var val = element.percentage_of_degrees
      if (val > max) {
        max = val
        univ = element.school_name
      }
    });
    return univ
  }

  getTopCity(major) {
    var city = ''
    var max = 0
    major.cities.forEach((element) => {
      if (element.major_population > max) {
        max = element.major_population
        city = element.location
      }
    })
    return city
  }

  getColumnName(column) {
    column = column.replace(new RegExp('_', 'g'), ' ');
    return column.charAt(0).toUpperCase() + column.substr(1).toLowerCase()
  }

  search() {
    if (this.state.query !== "") {
      this.makeRequest();
    }
  }

  makeRequest() {
    axios.get(citiesSearchUrl, {
      params: {
        q: this.state.query
      }
    }).then((response) => {
      this.setState({cities: response.data.data, snippetsCities: response.data.snippets}, () =>
        this.setState({citiesLoaded: true}))
      }).catch((error) =>
      this.setState({cities: [], errorCities: "No results for search", citiesLoaded: true})
    )

    axios.get(majorsSearchUrl, {
      params: {
        q: this.state.query
      }
    }).then((response) => {
      this.setState({majors: response.data.data, snippetsMajors: response.data.snippets}, () =>
        this.setState({majorsLoaded: true}))
      }).catch((error) =>
      this.setState({majors: [], errorMajors: "No results for search", majorsLoaded: true})
    )

    axios.get(universitiesSearchUrl, {
      params: {
        q: this.state.query
      }
    }).then((response) => {
      this.setState({universities: response.data.data, snippetsUniversities: response.data.snippets}, () =>
        this.setState({universitiesLoaded: true, value: 0}))
      }).catch((error) =>
      this.setState({universities: [], error: "No results for search", universitiesLoaded: true, value: 0})
    )
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  universities() {
    return (
      <div>
        {this.state.universitiesLoaded && 
        <div>
        <Divider style={{display: 'hidden', width: "70%", marginLeft: "auto", marginRight: "auto"}}/>
        <Typography style={{ width: "100%", textAlign: "center", marginTop: "2%", marginBottom: '2%'}} variant="h5">
            Search results in Universities
        </Typography>
        </div>} 
        {this.state.universitiesLoaded && this.state.universities.length > 0 ? 
        (<div>
          <Grid container spacing="24" justify="left"
            style={{
              marginLeft: 'auto',
              marginBottom: '1%',
              marginRight: '0',
              width: '100%',
            }}
            xs={10}>
            {this.state.universities.slice(this.state.offsetUniversities, this.end('universities')).map((university, idx) =>
              <Grid key={idx} item>
                <Card className="my-card" name="universityCard">
                  <CardActionArea href={`/universities/${university.school_name}`}>
                    <CardMedia
                      className="testCardMedia"
                      image={university.images[0]}>
                    </CardMedia>
                    <CardContent>
                      <Typography noWrap gutterBottom variant="h6" component="h2">
                        {university.school_name}
                      </Typography>
                      <ul>
                        <li>{university.city}, {university.state}</li>
                        <li>Admission Rate: {(university.admission_rate * 100).toLocaleString(undefined, {maximumFractionDigits: 2})}%</li>
                        <li>Undergrad population: {university.undergrad_size.toLocaleString(undefined, {minimumIntegerDigits: 1})}</li>
                      </ul>
                      {this.state.snippetsUniversities.length > 0 && 
                        <Highlighter
                        highlightClassName="highlight"
                        searchWords={[this.state.query]}
                        autoEscape={true}
                        textToHighlight={this.getColumnName(this.state.snippetsUniversities[idx].column) + ":" + this.state.snippetsUniversities[idx].snippet}/>
                      }                    
                      </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            )}
          </Grid>
          <Pagination
          style={{textAlign: "center", marginBottom: '1%'}}
          size="large"
          limit={9}
          offset={this.state.offsetUniversities}
          total={this.state.universities.length}
          onClick={(e, offset) => this.handleClick(offset, 'offsetUniversities')}
          />
        </div>) : (<Typography style={{width: "100%", textAlign: "center", marginBottom: "2%"}}variant="h5">{this.state.errorUniversities}</Typography>)}
      </div>
    )
  }

  cities() {
    return (
      <div>
        {this.state.citiesLoaded && 
        <div>
          <Divider style={{width: "70%", marginLeft: "auto", marginRight: "auto"}}/>
          <Typography style={{ width: "100%", textAlign: "center", marginTop: '2%', marginBottom: '2%'}} variant="h5">
              Search results in Cities
          </Typography>
        </div>}
        {this.state.citiesLoaded &&  this.state.cities.length > 0 ? (
        <div>
          <Grid container spacing="24" justify="left"
            style={{
              marginLeft: 'auto',
              marginBottom: '1%',
              marginRight: '0',
              width: '100%',
            }}
            xs={10}>
            {this.state.cities.slice(this.state.offsetCities, this.end('cities')).map((city, idx) => 
              <Grid key={idx} item>
                <Card className="my-card" name="cityCard">
                  <CardActionArea href={`/cities/${city.name}_${city.state}`}>
                    <CardMedia
                      className="testCardMedia"
                      image={city.images[0]}>
                    </CardMedia>
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2" noWrap>
                        {city.name}, {city.state}
                      </Typography>
                        <ul>
                          <li>Population: {city.pop_total.toLocaleString(undefined, {maximumFractionDigits: 0})} </li>
                          <li>Median Income: ${city.income.toLocaleString(undefined, {maximumFractionDigits: 0})} </li>
                          <li>Median Property Value: ${city.median_property_value.toLocaleString(undefined, {maximumFractionDigits: 0})}  </li>
                        </ul>
                      {this.state.snippetsCities.length > 0 && 
                        <Highlighter
                        highlightClassName="highlight"
                        searchWords={[this.state.query]}
                        autoEscape={true}
                        textToHighlight={this.getColumnName(this.state.snippetsCities[idx].column) + ":" + this.state.snippetsCities[idx].snippet}/>
                      }                    
                      </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            )}
          </Grid>

          <Pagination
            style={{textAlign: "center", marginBottom: '1%'}}
            size="large"
            limit={9}
            offset={this.state.offsetCities}
            total={this.state.cities.length}
            onClick={(e, offset) => this.handleClick(offset, 'offsetCities')}
          />
        </div>) : (<Typography style={{width: "100%", textAlign: "center", marginBottom: "2%"}}variant="h5">{this.state.errorCities}</Typography>)}
      </div>
    )
  }

  majors() {
    return (
      <div>
        {this.state.majorsLoaded && 
        <div>
          <Divider style={{width: "70%", marginLeft: "auto", marginRight: "auto"}}/>
          <Typography style={{ width: "100%", textAlign: "center", marginTop: '2%', marginBottom: '2%'}} variant="h5">
              Search results in Majors
          </Typography>
        </div>}
        {this.state.majorsLoaded && this.state.majors.length > 0 ? 
          (<div>
          <Grid container spacing="24" justify="left"
            style={{
              marginLeft: 'auto',
              marginBottom: '0%',
              marginRight: '0',
              width: '100%',
            }}
            xs={10}>
            {this.state.majors.slice(this.state.offsetMajors, this.end('majors')).map((major, idx) =>
              <Grid key={idx} item>
                <Card className="my-card" name="majorCard">
                  <CardActionArea href={`/majors/${major.major}`}>
                    <CardMedia
                      className="testCardMedia"
                      image={major.images[0]}>
                    </CardMedia>
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                        {major.major.replace(/\w\S*/g, function(txt) {
                          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                        })}
                      </Typography>
                      <ul>
                        <li><Typography gutterBottom component="p" noWrap>Field of study: {major.area_of_study.replace('_', ' ').replace(/\w\S*/g, function(txt) {
                          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                        })}</Typography></li>
                        <li><Typography gutterBottom component="p" noWrap>Top City: {this.getTopCity(major)}</Typography></li>
                        <li><Typography gutterBottom component="p" noWrap>Top University: {this.getTopUniversity(major)}</Typography></li>
                        <li><Typography gutterBottom component="p" noWrap>Starting salary: ${major.starting_salary.toLocaleString(undefined, {maximumFractionDigits: 0})}</Typography></li>
                      </ul>
                      {this.state.snippetsMajors.length > 0 && 
                        <Highlighter
                        highlightClassName="highlight"
                        searchWords={[this.state.query]}
                        autoEscape={true}
                        textToHighlight={this.getColumnName(this.state.snippetsMajors[idx].column) + ":" + this.state.snippetsMajors[idx].snippet}/>
                      }                    
                      </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            )}
          </Grid>
          <Pagination 
          style={{textAlign: "center", marginBottom: '1%'}} 
          size="large" 
          limit={9} 
          offset={this.state.offsetMajors} 
          total={this.state.majors.length} 
          onClick={(e, offset) => this.handleClick(offset, 'offsetMajors')}/>
        </div>) :(<Typography style={{width: "100%", textAlign: "center", marginBottom: "2%"}}variant="h5">{this.state.errorMajors}</Typography>)}
      </div>
    )
  }

  render() {
    const { theme } = this.props;
    return (
      <div className="root">
      <AppBar position="relative" color="primary" style={{marginTop: '-40px', boxShadow: '0 6px 5px -3px gray'}}>
          <Tabs
            value={this.state.value}
            onChange={this.handleChange}
            indicatorColor="secondary"
            textColor="inherit"
            className="tabs"
            variant="fullWidth"
          >
            <Tab classes={{root: "tabs"}} label="Universities" />
            <Tab classes={{root: "tabs"}} label="Cities" />
            <Tab classes={{root: "tabs"}} label="Majors" />
          </Tabs>
        </AppBar>
        <SwipeableViews
          axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={this.state.value}
          onChangeIndex={this.handleChangeIndex}
          animateHeight
        >
          <div>{this.universities()}</div>
          <div>{this.cities()}</div>
          <div>{this.majors()}</div>
        </SwipeableViews>
      </div>
    );
  }
}

Search.propTypes = {
  theme: PropTypes.object.isRequired,
};

export default withTheme()(Search);
