import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import { CardActionArea, CardMedia, Typography, CardContent, Grid } from '@material-ui/core';
import './Cities.css';
import Pagination from 'material-ui-flat-pagination';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Search from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import { IconButton } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Highlighter from "react-highlight-words";

const citiesUrl = 'https://api.myfuture.education/cities';
const citiesSearchUrl = 'https://api.myfuture.education/search/cities';
const states = ["Alabama", "Alaska", "Arizona", "California", "Colorado", 
                "Connecticut", "District of Columbia", "Florida", "Georgia", 
                "Hawaii", "Illinois", "Indiana", "Iowa", "Louisiana", 
                "Maryland", "Massachusetts", "Michigan", "Minnesota", 
                "Mississippi", "New York", "North Carolina", "Ohio", 
                "Oklahoma", "Pennsylvania", "Tennessee", "Texas", "Utah", 
                "Virginia", "Washington", "Wisconsin"]

const rangesPopulation = [{min: "", max: "35000"}, 
                {min: "35001", max: "100000"}, 
                {min: "100001", max: "500000"}, 
                {min: "500001", max: "1000000"}, 
                {min: "1000001", max: "5000000"},
                {min: "5000001", max: ""}];

const rangesPropertyValue = [{min: "", max: "35000"}, 
                {min: "35001", max: "100000"}, 
                {min: "100001", max: "50000"}, 
                {min: "500001", max: "1000000"}, 
                {min: "1000001", max: "5000000"},
                {min: "5000001", max: ""}];

const rangesIncome = [{min: "", max: "30000"}, 
                {min: "30001", max: "50000"}, 
                {min: "50001", max: "70000"}, 
                {min: "70001", max: "90000"}, 
                {min: "90000", max: ""}];

class Cities extends Component{
  constructor(props) {
    super(props)
    this.state = {
      offset: 0,
      cities: null,
      snippets: [],
      loaded: false,
      sort_by: "",
      state: "",
      population: "",
      median_income: "",
      params: {},
      filters: [],
      mins:[],
      maxes:[],
      error: "",
      searchQuery: ''
    }
  }
  componentDidMount() {
    axios.get(citiesUrl).then((response) => {
      this.setState({cities: response.data.data}, () =>
        this.setState({loaded: true}))
      }).catch((error) =>
      this.setState({cities: [], error: "No cities found"})
    )
    document.title = 'Cities | My Future Education';
  }
  
  handleClick(offset) {
    this.setState({offset})
    window.scrollTo(0, 0)
  }

  end() {
    return this.state.offset + 9 >= this.state.cities.length ? this.state.cities.length : this.state.offset + 9;
  }

  getColumnName(column) {
    column = column.replace(new RegExp('_', 'g'), ' ');
    return column.charAt(0).toUpperCase() + column.substr(1).toLowerCase()
  }

  search = (event, button) => {
    if (button === 1 || event.keyCode === 13) {
      this.state.params.q=this.state.searchQuery;
      this.makeRequest();
    }
  }

  sortBy = event => {
    this.setState({[event.target.name]: event.target.value});
    this.state.params.order_by = 'name';
    this.state.params.order = event.target.value === 0 ? 'ascending' : 'descending'
    this.makeRequest();
  }

  filterPopulation = event => {
    this.setState({[event.target.name]: event.target.value});
    delete this.state.params.min;
    delete this.state.params.max;
    var range = event.target.value !== -1 ? rangesPopulation[event.target.value] : {min: "", max: ""}
    var idx = this.state.filters.indexOf('pop_total');
    var state_idx = this.state.filters.indexOf('state');
    var num_idx = idx

    if (state_idx !== -1 && num_idx > state_idx){
      num_idx = num_idx - 1
    }

    if (event.target.value === -1 && idx !== -1) {
      this.state.filters.splice(idx, 1);
      this.state.mins.splice(num_idx, 1);
      this.state.maxes.splice(num_idx, 1);
    } else {
      if (idx === -1) {
        this.state.filters.push('pop_total')
        this.state.mins.push(range.min)
        this.state.maxes.push(range.max)
      } else {
        this.state.filters.splice(idx, 1);
        this.state.mins.splice(num_idx, 1);
        this.state.maxes.splice(num_idx, 1);
        this.state.filters.push('pop_total')
        this.state.mins.push(range.min)
        this.state.maxes.push(range.max)
      }
    }
    if (this.state.filters.length > 0) {
      this.state.params.filters = this.state.filters.join(',');
      this.state.params.mins = this.state.mins.join(',');
      this.state.params.maxes = this.state.maxes.join(',');
    } else  {
      delete this.state.params.filters;
      delete this.state.params.mins;
      delete this.state.params.maxes;
    }
    this.makeRequest()
  };

  filterState = event => {
    this.setState({[event.target.name]: event.target.value});
    var idx = this.state.filters.indexOf('state');
    if (event.target.value === -1) {
      delete this.state.params.state;
      if (idx !== -1) {
        this.state.filters.splice(idx, 1);
      }
    } else {
      this.state.params.state=event.target.value;
      if (idx === -1) {
        this.state.filters.push('state');
      }
    }
    if (this.state.filters.length > 0) {
      this.state.params.filters = this.state.filters.join(',');
      this.state.params.mins = this.state.mins.join(',');
      this.state.params.maxes = this.state.maxes.join(',');
    } else  {
      delete this.state.params.filters;
      delete this.state.params.mins;
      delete this.state.params.maxes;
    }
    this.makeRequest()
  }

  filterIncome = event => {
    this.setState({[event.target.name]: event.target.value});
    delete this.state.params.min;
    delete this.state.params.max;
    var range = event.target.value !== -1 ? rangesIncome[event.target.value] : {min: "", max: ""}
    var idx = this.state.filters.indexOf('income');
    var state_idx = this.state.filters.indexOf('state');
    var num_idx = idx

    if (state_idx !== -1 && num_idx > state_idx){
      num_idx = num_idx - 1
    }
    if (event.target.value === -1 && idx !== -1) {
      this.state.filters.splice(idx, 1);
      this.state.mins.splice(num_idx, 1);
      this.state.maxes.splice(num_idx, 1);
    } else {
      if (idx === -1) {
        this.state.filters.push('income')
        this.state.mins.push(range.min)
        this.state.maxes.push(range.max)
      } else {
        this.state.filters.splice(idx, 1);
        this.state.mins.splice(num_idx, 1);
        this.state.maxes.splice(num_idx, 1);
        this.state.filters.push('income')
        this.state.mins.push(range.min)
        this.state.maxes.push(range.max)
      }
    }
    if (this.state.filters.length > 0) {
      this.state.params.filters = this.state.filters.join(',');
      this.state.params.mins = this.state.mins.join(',');
      this.state.params.maxes = this.state.maxes.join(',');
    } else  {
      delete this.state.params.filters;
      delete this.state.params.mins;
      delete this.state.params.maxes;
    }
    this.makeRequest()
  }

  makeRequest() {
    axios.get(citiesSearchUrl, {
      params: this.state.params
    }).then((response) => {
      this.setState({cities: response.data.data, snippets: response.data.snippets})
    }).catch((error) => this.setState({cities: [], error: "No results for search"}))
  }

  render() {
    if (this.state.loaded) {
    return (
      <div className="root">
        <Typography variant="h3" align="center" gutterBottom>
            Cities
        </Typography>
        <div className={"search-filter"}>
          <Grid container spacing={8}>
            <Grid item xs={12}>
              <TextField
                style={{width: "100%"}}
                id="outlined-bare"
                placeholder="Search..."
                margin="normal"
                autoComplete="false"
                onChange={(e) => {this.setState({searchQuery: e.target.value})}}
                onKeyDown={(e) => {this.search(e, 0)}}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton className={'icon'} onClick={(e) => {this.search(e, 1)}}>
                      <Search />
                      </IconButton>
                    </InputAdornment>
                  ),
                  classes: {
                    root: 'cssLabel',
                  },
                }}
              />
            </Grid>
            
            <Grid container item xs={12} spacing={8}>
              <Grid item xs={12} sm={6} md={3}>
                <FormControl style={{width: "100%"}}>
                  <InputLabel htmlFor="sort-by">Sort by name</InputLabel>
                  <Select
                    value={this.state.sort_by}
                    onChange={this.sortBy}
                    inputProps={{
                      name: 'sort_by',
                      id: 'sort-by',
                    }}
                  >
                    <MenuItem value={0}>Ascending</MenuItem>
                    <MenuItem value={1}>Descending</MenuItem>
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={6} md={3}>
                <FormControl style={{width: "100%"}}>
                  <InputLabel htmlFor="sort-by">State</InputLabel>
                  <Select
                    value={ this.state.state == -1 ? "" : this.state.state}
                    onChange={this.filterState}
                    inputProps={{
                      name: 'state',
                      id: 'state',
                    }}
                  >
                    <MenuItem value={-1}><em>Any</em></MenuItem>
                    {states.map((state, idx) => 
                      <MenuItem value={states[idx]}>{state}</MenuItem>)}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={6} md={3}>
                <FormControl style={{width: "100%"}}>
                  <InputLabel htmlFor="population">Population</InputLabel>
                  <Select
                    value={ this.state.population == -1 ? "" : this.state.population}
                    onChange={this.filterPopulation}
                    inputProps={{
                      name: 'population',
                      id: 'population',
                    }}
                  >
                    <MenuItem value={-1}><em>Any</em></MenuItem>
                    <MenuItem value={0}>&lt; 35K</MenuItem>
                    <MenuItem value={1}>35K - 100K</MenuItem>
                    <MenuItem value={2}>100K - 500K</MenuItem>
                    <MenuItem value={3}>500K - 1M</MenuItem>
                    <MenuItem value={4}>1M - 5M</MenuItem>
                    <MenuItem value={5}>&gt; 5M</MenuItem>
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={6} md={3}>
                <FormControl style={{width: "100%"}}>
                  <InputLabel htmlFor="median-income">Median Income</InputLabel>
                  <Select
                    value={ this.state.median_income == -1 ? "" : this.state.median_income}
                    onChange={this.filterIncome}
                    inputProps={{
                      name: 'median_income',
                      id: 'median-income',
                    }}
                  >
                    <MenuItem value={-1}><em>Any</em></MenuItem>
                    <MenuItem value={0}>&lt; $30K</MenuItem>
                    <MenuItem value={1}>$30K - $50K</MenuItem>
                    <MenuItem value={2}>$50K - $70K</MenuItem>
                    <MenuItem value={3}>$70K - $90K</MenuItem>
                    <MenuItem value={4}>&gt; $90K</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </Grid>
        </div>
        {this.state.cities.length > 0 ? (
        <div>
          <Grid container spacing="24" justify="left"
            style={{
              marginLeft: 'auto',
              marginBottom: '1%',
              marginRight: '0',
              width: '100%',
            }}
            xs={10}>
            {this.state.cities.slice(this.state.offset, this.end()).map((city, idx) => 
              <Grid item>
                <Card className="my-card" name="cityCard">
                  <CardActionArea href={`/cities/${city.name}_${city.state}`}>
                    <CardMedia
                      className="testCardMedia"
                      image={city.images[0]}>
                    </CardMedia>
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2" noWrap>
                        {city.name}, {city.state}
                      </Typography>
                      <Typography gutterBottom component="p">
                        <ul>
                          <li>Population: {city.pop_total.toLocaleString(undefined, {maximumFractionDigits: 0})} </li>
                          <li>Median Income: ${city.income.toLocaleString(undefined, {maximumFractionDigits: 0})} </li>
                          <li>Median Property Value: ${city.median_property_value.toLocaleString(undefined, {maximumFractionDigits: 0})}  </li>
                        </ul>
                      </Typography>
                      {this.state.snippets.length > 0 && 
                      <Highlighter
                        highlightClassName="highlight"
                        searchWords={[this.state.searchQuery]}
                        autoEscape={true}
                        textToHighlight={this.getColumnName(this.state.snippets[idx].column) + ":" + this.state.snippets[idx].snippet}/>
                      }
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            )}
          </Grid>

          <Pagination
            style={{textAlign: "center", marginBottom: '1%'}}
            size="large"
            limit={9}
            offset={this.state.offset}
            total={this.state.cities.length}
            onClick={(e, offset) => this.handleClick(offset)}
          />
        </div>) : (<Typography style={{width: "100%", textAlign: "center"}}variant="h4">{this.state.error}</Typography>)}
      </div>
    );
    }
    else {
      return null
    }
  }
}

export default Cities
