import React, { Component } from 'react';
import { Typography, CardMedia, CardContent, Card, Divider, CardActionArea } from '@material-ui/core';
import PropTypes, { func } from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Carousel from 'react-bootstrap/Carousel';
import './CitiesInstance.css';
import {Pie, Bar} from 'react-chartjs-2';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import purple from '@material-ui/core/colors/purple'
import blue from '@material-ui/core/colors/blue'
import pink from '@material-ui/core/colors/pink'
import red from '@material-ui/core/colors/red'
import teal from '@material-ui/core/colors/teal'
import deepOrange from '@material-ui/core/colors/deepOrange'
import lime from '@material-ui/core/colors/lime'
import axios from 'axios'

const styles = theme => ({
  root: {
    width: '50%',
    margin: 'auto'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

const citiesUrl = 'https://api.myfuture.education/cities';
const majorsUrl = 'https://api.myfuture.education/majors';
const universitiesUrl = 'https://api.myfuture.education/universities';

class CitiesInstance extends Component {
  constructor(props) {
    super(props);
    this.state={
      city_data: null,
      loaded: false,
      universities: [],
      majors: [],
      incomeLabels: null
    }
  }

  componentDidMount() {
    var cityState = this.props.match.params.city.split('_')
    var city = cityState[0]
    var state = cityState[1]
    axios.get(citiesUrl, {
      params: {
        city: city,
        state: state
      }
    }).then((response) => 
      {
      this.setState({city_data: response.data}, () => {
        var labels = []
        if (this.state.city_data.income_white > 0) {
          labels.push("White");
        }

        if (this.state.city_data.income_asian > 0) {
          labels.push("Asian");
        }

        if (this.state.city_data.income_black > 0) {
          labels.push("Black");
        }

        if (this.state.city_data.income_native > 0) {
          labels.push("Native");
        }

        if (this.state.city_data.income_hawaiian > 0) {
          labels.push("Hawaiian");
        }

        this.setState({incomeLabels: labels})

        this.state.city_data.universities.forEach((element) =>
          axios.get(universitiesUrl, {
            params: {
              name: element,
              columns: 'school_name,admission_rate,undergrad_size,images'
            }
          }).then((response) => {
            const updateList = this.state.universities.slice()
            updateList.push(response.data)
            this.setState({universities: updateList})
          })
        )
        this.state.city_data.majors_and_populations.forEach((element) => {
          axios.get(majorsUrl, {
            params: {
              major: element.major,
              columns: 'major,images'
            }
          }).then((response) => {
            const updateList = this.state.majors.slice()
            updateList.push(response.data)
            this.setState({majors: updateList})
          })
      })
      this.setState({loaded: true})
    })
  }).catch((error) =>
      console.log(error)
    )
    document.title = `${city}, ${state} | My Future Education`
  }

  getAgeData() {
    var ageData = {
      labels: [
        ' < 18',
        ' 18-24',
        ' 25-34',
        ' 35-44',
        ' 45-54',
        ' 55-64',
        ' 65 <'
      ],
      datasets: [{
        data: [
          this.state.city_data.pop_lt_18,
          this.state.city_data.pop_18_24,
          this.state.city_data.pop_25_34,
          this.state.city_data.pop_35_44,
          this.state.city_data.pop_45_54,
          this.state.city_data.pop_55_64,
          this.state.city_data.pop_gt_65
        ],
        backgroundColor: [
          purple[400],
          blue[400],
          red[400],
          pink[400],
          deepOrange[400],
          teal[400],
          lime[400]
        ],
        hoverBackgroundColor: [
          purple[400],
          blue[400],
          red[400],
          pink[400],
          deepOrange[400],
          teal[400],
          lime[400]
        ]
      }]
    }
    return ageData;
  }
  getHispanicData() {
    var hispanicPopData = {
      labels: [
        'Hispanic',
        'Non-Hispanic',
      ],
      datasets: [{
        data: [
          this.state.city_data.pop_hispanic,
          this.state.city_data.pop_total - this.state.city_data.pop_hispanic,
        ],
        backgroundColor: [
          blue[400],
          red[400],
        ],
        hoverBackgroundColor: [
          blue[400],
          red[400],
        ]
      }]
    }
    return hispanicPopData;
  }

  getEthnicityData() {
    var ethnicityPopData = {
      labels: [
        'Asian',
        'Black',
        'Hawaiian',
        'White',
        'Native',
        'Other',
      ],
      datasets: [{
        data: [
          this.state.city_data.pop_asian,
          this.state.city_data.pop_black,
          this.state.city_data.pop_hawaiian,
          this.state.city_data.pop_white,
          this.state.city_data.pop_native,
          this.state.city_data.pop_other,
        ],
        backgroundColor: [
          purple[400],
          blue[400],
          red[400],
          pink[400],
          deepOrange[400],
          teal[400],
        ],
        hoverBackgroundColor: [
          purple[400],
          blue[400],
          red[400],
          pink[400],
          deepOrange[400],
          teal[400],
        ]
      }]
    }
    return ethnicityPopData;
  }

  getIncomeData() {
    var incomeData = {
      labels: this.state.incomeLabels,
      datasets: [
        {
          label: 'Income',
          backgroundColor: pink[300],
          borderColor: pink[300],
          borderWidth: 1,
          hoverBackgroundColor: pink[200],
          hoverBorderColor: pink[200],
          data: [
            this.state.city_data.income_white, 
            this.state.city_data.income_asian, 
            this.state.city_data.income_black, 
            this.state.city_data.income_native, 
            this.state.city_data.income_hawaiian
          ]
        }
      ]
    }
    return incomeData;
  }
  

  render() {
    const { classes } = this.props;
    if (this.state.loaded) {
      return(
        <div className={classes.root}>
          <Typography variant="h3" align="center" gutterBottom>
            {`${this.state.city_data.name}, ${this.state.city_data.state}`}
          </Typography>

          <Carousel className="Carousel-container">
          {this.state.city_data.images.map((url) =>
            <Carousel.Item>
              <div style={{width: "100%", paddingBottom: "56.25%", position: "relative", marginTop: "2%", marginBottom: "2%"}}>
                <img
                  className="Carousel-content"
                  src={url}
                  alt="City"
                />
              </div>
            </Carousel.Item>
          )}
          </Carousel>
          <div className="Charts">
            <ExpansionPanel>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
                <Typography className={classes.heading}>
                  Population
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <div style={{width: '100%'}}>
                  <Typography variant="subtitle1" align="left">
                    Total population: {this.state.city_data.pop_total.toLocaleString(undefined, {minimumIntegerDigits: 1})}
                  </Typography>
                  <Typography variant="subtitle1" align="left">
                    Average citizen age: {this.state.city_data.age.toLocaleString(undefined, {minimumFractionDigits: 1})} years
                  </Typography>
                  <div style={{paddingTop: "20px", paddingBottom: "30px"}}>
                    <Typography variant="h6" align="center">
                      Population by age
                    </Typography>
                    <Pie data={ this.getAgeData() } />
                  </div>
                  <div style={{paddingTop: "20px", paddingBottom: "30px"}}>
                    <Typography variant="h6" align="center">
                      Population by Race
                    </Typography>
                    <Pie data={ this.getEthnicityData() } />
                  </div>
                  <div style={{paddingTop: "20px", paddingBottom: "30px"}}>
                    <Typography variant="h6" align="center">
                      Population of Hispanics
                    </Typography>
                    <Pie data={ this.getHispanicData() } />
                  </div>
                </div>
              </ExpansionPanelDetails>
            </ExpansionPanel>
            <ExpansionPanel>
              <ExpansionPanelSummary expandIcon={<ExpandMoreIcon/>}>
                <Typography className={classes.heading}>
                  Income
                </Typography>
              </ExpansionPanelSummary>
              <ExpansionPanelDetails>
                <div style={{width: '100%'}}>
                  <Typography variant="subtitle1" align="left">
                  Average income: ${this.state.city_data.income.toLocaleString(undefined, {minimumFractionDigits: 2})}
                  </Typography>
                  <Typography variant="subtitle1" align="left">
                    Median property value: ${this.state.city_data.median_property_value.toLocaleString(undefined, {minimumFractionDigits: 2})}
                  </Typography>

                  <div style={{paddingTop: "20px", paddingBottom: "30px"}}>
                    <Typography variant="h6" align="center">
                      Income by Race
                    </Typography>
                    <Bar 
                    data={ this.getIncomeData()} />
                  </div>
                </div>
              </ExpansionPanelDetails>
            </ExpansionPanel>
          </div>
          <Divider/>
          <Typography style={{marginTop: '2%', marginBottom: '2%'}} variant="h5">
            Universities in {this.state.city_data.name}
          </Typography>
          {this.state.universities.map((university) => 
          <CardActionArea href={`/universities/${university.school_name}`}>
            <Card className="Card">
              <CardMedia
                image={university.images[0]}
                className="Card-Media"
              />
              <div className="Card-Details">
                <CardContent>
                  <Typography variant="h5">
                    {university.school_name}
                  </Typography>
                  <Typography variant="subtitle1">
                    Admission rate: {university.admission_rate !== null ? (university.admission_rate * 100).toLocaleString(undefined, {maximumFractionDigits: 2}) + "%" : "Not available"}
                  </Typography>
                  <Typography variant="subtitle1">
                    Undergrad student body size: {university.undergrad_size.toLocaleString(undefined, {maximumFractionDigits: 0})}
                  </Typography>
                </CardContent>
              </div>
            </Card>
          </CardActionArea>
          )}
          <Divider/>
          <Typography style={{marginTop: '2%', marginBottom: '2%'}} variant="h5">
            Popular Majors in {this.state.city_data.name}
          </Typography>
          {this.state.majors.map((major) =>
          <CardActionArea href={`/majors/${major.major}`}>
            <Card className="Card">
              
                <CardMedia
                  image={major.images[0]}
                  className="Card-Media"
                />
                <div className="Card-Details">
                  <CardContent>
                    <Typography variant="h5">
                      {major.major.replace(/\w\S*/g, function(txt) {
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                        })}
                    </Typography>
                    <Typography gutterBottom variant="subtitle1" noWrap>Field of study: {major.area_of_study.replace('_', ' ').replace(/\w\S*/g, function(txt) {
                    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    })}</Typography>
                    <Typography gutterBottom variant="subtitle1" noWrap>Starting salary: ${major.starting_salary.toLocaleString(undefined, {maximumFractionDigits: 0})}</Typography>
                  </CardContent>
                </div>
              </Card>
            </CardActionArea>
          )}
        </div>
      );
    }
    else {
      return null
    }
  }
}

CitiesInstance.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CitiesInstance)
