import React, { Component } from 'react';
import Card from '@material-ui/core/Card';
import { CardActionArea, CardMedia, Typography, CardContent, Grid } from '@material-ui/core';
import './Majors.css';
import Pagination from 'material-ui-flat-pagination';
import axios from 'axios';
import TextField from '@material-ui/core/TextField';
import Search from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import { IconButton } from '@material-ui/core';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Highlighter from "react-highlight-words";


const majorsUrl = 'https://api.myfuture.education/majors';
const majorsSearchUrl = 'https://api.myfuture.education/search/majors';
const ranges = [{min: null, max: 40000}, 
                {min: 40001, max: 55000}, 
                {min: 55001, max: null}];


class Majors extends Component{
  constructor(props) {
    super(props)
    this.state = {
      offset: 0,
      majors: null,
      snippets: [],
      loaded: false,
      searchQuery:  "",
      field_of_study: "",
      starting_salary: "",
      params: {},
      filters: [],
      error: ""
    }
  }

  componentDidMount() {

    axios.get(majorsUrl).then((response) => {
      this.setState({majors: response.data.data}, () =>
        this.setState({loaded: true}))
      }).catch((error) =>
      this.setState({majors: [], error: "No majors found"})
    )

    document.title = 'Majors | My Future Education';
  }

  handleClick(offset) {
    this.setState({offset})
    window.scrollTo(0, 0)
  }

  end() {
    return this.state.offset + 9 >= this.state.majors.length ? this.state.majors.length : this.state.offset + 9;
  }

  getTopUniversity(major) {
    var univ = ''
    var max = 0
    major.universities.forEach((element) => {
      var val = element.percentage_of_degrees
      if (val > max) {
        max = val
        univ = element.school_name
      }
    });
    return univ
  }

  getTopCity(major) {
    var city = ''
    var max = 0
    major.cities.forEach((element) => {
      if (element.major_population > max) {
        max = element.major_population
        city = element.location
      }
    })
    return city
  }

  getColumnName(column) {
    column = column.replace(new RegExp('_', 'g'), ' ');
    return column.charAt(0).toUpperCase() + column.substr(1).toLowerCase()
  }

  search = (event, button) => {
    if (button === 1 || event.keyCode === 13) {
      this.state.params.q=this.state.searchQuery;
      this.makeRequest();
    }
  }

  filterStartingSalary = event => {
    this.setState({[event.target.name]: event.target.value});
    delete this.state.params.mins;
    delete this.state.params.maxes;
    var range = event.target.value !== -1 ? ranges[event.target.value] : {min: null, max: null}
    var idx = this.state.filters.indexOf('starting_salary');
    if (event.target.value === -1) {
      if (idx !== -1) {
        this.state.filters.splice(idx, 1);
      }
    } else {
      if (range.min !== null) {
        this.state.params.mins = range.min;
      }
      if (range.max !== null) {
        this.state.params.maxes = range.max;
      }
      if (idx === -1) {
        this.state.filters.push('starting_salary')
      }
    }
    if (this.state.filters.length > 0) {
      this.state.params.filters = this.state.filters.join(',');
    } else  {
      delete this.state.params.filters;
    }
    this.makeRequest()
  };

  filterFieldOfStudy = event => {
    this.setState({[event.target.name]: event.target.value});
    var idx = this.state.filters.indexOf('area_of_study');
    if (event.target.value === 'Any') {
      delete this.state.params.field;
      if (idx !== -1) {
        this.state.filters.splice(idx, 1);
      }
    } else {
      this.state.params.field=event.target.value;
      if (idx === -1) {
        this.state.filters.push('area_of_study');
      }
    }
    if (this.state.filters.length > 0) {
      this.state.params.filters = this.state.filters.join(',');
    } else  {
      delete this.state.params.filters;
    }
    this.makeRequest()
  }

  makeRequest() {
    axios.get(majorsSearchUrl, {
      params: this.state.params
    }).then((response) => {
      this.setState({majors: response.data.data, snippets: response.data.snippets})
    }).catch((error) => this.setState({majors: [], error: "No results for search"}))
  }

  render() {
    if (this.state.loaded) {
    return (
      <div className="root">
        <Typography variant="h3" align="center" gutterBottom>
            Majors
        </Typography>
        <div className={"search-filter"}>
          <Grid container spacing={8}>
            <Grid item sm={12} md={6} style={{width: "100%"}}>
              <TextField
                style={{width: "100%"}}
                id="outlined-bare"
                placeholder="Search..."
                margin="normal"
                autoComplete="false"
                onChange={(e) => {this.setState({searchQuery: e.target.value})}}
                onKeyDown={(e) => this.search(e, 0)}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton className={'icon'} onClick={(e) => this.search(e, 1)}>
                      <Search />
                      </IconButton>
                    </InputAdornment>
                  ),
                  classes: {
                    root: 'cssLabel',
                  },
                }}
              />
            </Grid>
            
            <Grid container item sm={12} md={6} spacing={8}>
              <Grid item xs={12} sm={6}>
                <FormControl style={{width: "100%"}}>
                  <InputLabel htmlFor="field-of-study">Field of study</InputLabel>
                  <Select
                    value={ this.state.field_of_study === "Any" ? "" : this.state.field_of_study}
                    onChange={this.filterFieldOfStudy}
                    inputProps={{
                      name: 'field_of_study',
                      id: 'field-of-study',
                    }}
                  >
                    <MenuItem value={"Any"}><em>Any</em></MenuItem>
                    <MenuItem value={"Architecture"}>Architecture</MenuItem>
                    <MenuItem value={"Business"}>Business</MenuItem>
                    <MenuItem value={"Communication"}>Communication</MenuItem>
                    <MenuItem value={"Engineering"}>Engineering</MenuItem>
                    <MenuItem value={"Fine Arts"}>Fine Arts</MenuItem>
                    <MenuItem value={"Liberal Arts"}>Liberal Arts</MenuItem>
                    <MenuItem value={"Science"}>Science</MenuItem>
                    <MenuItem value={"Social Work"}>Social Work</MenuItem>
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={6}>
                <FormControl style={{width: "100%"}}>
                  <InputLabel htmlFor="starting-salary">Starting salary</InputLabel>
                  <Select
                    value={ this.state.starting_salary === -1 ? "" : this.state.starting_salary}
                    onChange={this.filterStartingSalary}
                    inputProps={{
                      name: 'starting_salary',
                      id: 'starting-salary',
                    }}
                  >
                    <MenuItem value={-1}><em>Any</em></MenuItem>
                    <MenuItem value={0}>&lt; $40K</MenuItem>
                    <MenuItem value={1}>$40K - $55K</MenuItem>
                    <MenuItem value={2}>&gt; $55K</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
            </Grid>
          </Grid>
        </div>
        {this.state.majors.length > 0 ? 
          (<div>
          <Grid container spacing="24" justify="left"
            style={{
              marginLeft: 'auto',
              marginRight: '0',
              marginBottom: '1%',
              width: '100%',
            }}
            xs={10}>
            {this.state.majors.slice(this.state.offset, this.end()).map((major, idx) =>
              <Grid item>
                <Card className="my-card" name="majorCard">
                  <CardActionArea href={`/majors/${major.major}`}>
                    <CardMedia
                      className="testCardMedia"
                      image={major.images[0]}>
                    </CardMedia>
                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                        {major.major.replace(/\w\S*/g, function(txt) {
                          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                        })}
                      </Typography>
                      <ul>
                        <li><Typography gutterBottom component="p" noWrap>Field of study: {major.area_of_study.replace('_', ' ').replace(/\w\S*/g, function(txt) {
                          return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                        })}</Typography></li>
                        <li><Typography gutterBottom component="p" noWrap>Top City: {this.getTopCity(major)}</Typography></li>
                        <li><Typography gutterBottom component="p" noWrap>Top University: {this.getTopUniversity(major)}</Typography></li>
                        <li><Typography gutterBottom component="p" noWrap>Starting salary: ${major.starting_salary.toLocaleString(undefined, {maximumFractionDigits: 0})}</Typography></li>
                      </ul>
                      {this.state.snippets.length > 0 && 
                        <Highlighter
                        highlightClassName="highlight"
                        searchWords={[this.state.searchQuery]}
                        autoEscape={true}
                      textToHighlight={this.getColumnName(this.state.snippets[idx].column) + ":" + this.state.snippets[idx].snippet}/>}
                    </CardContent>
                  </CardActionArea>
                </Card>
              </Grid>
            )}
          </Grid>
          <Pagination 
          style={{textAlign: "center", marginBottom: '1%'}} 
          size="large" 
          limit={9} 
          offset={this.state.offset} 
          total={this.state.majors.length} 
          onClick={(e, offset) => this.handleClick(offset)}/>
        </div>) :(<Typography style={{width: "100%", textAlign: "center"}}variant="h4">{this.state.error}</Typography>)}
      </div>
    );
  }
  else {
    return null
  }
  }
  
}

export default Majors
