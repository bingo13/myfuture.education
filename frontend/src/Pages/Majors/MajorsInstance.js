import React, { Component } from 'react';
import { Typography, CardMedia, CardContent, Card, Divider, CardActionArea } from '@material-ui/core';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import './MajorsInstance.css';
import axios from 'axios'


const styles = theme => ({
  root: {
    width: '50%',
    margin: 'auto'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
});

const citiesUrl = 'https://api.myfuture.education/cities';
const majorsUrl = 'https://api.myfuture.education/majors';
const universitiesUrl = 'https://api.myfuture.education/universities';

class MajorsInstance extends Component {
  constructor(props) {
    super(props);
    this.state={
      name: null, 
      major_data: null,
      loaded: false,
      cities: [],
      universities: []
    }
  }

  componentDidMount() {
    var major = this.props.match.params.major
    var majorName = major.replace(/\w\S*/g, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    })
    axios.get(majorsUrl, {
      params: {
        major: major,
      }
    }).then((response) => 
      {
      this.setState({major_data: response.data}, () => {
        this.state.major_data.cities.forEach((element) =>
          axios.get(citiesUrl, {
            params: {
              city: element.location.split(',')[0],
              columns: 'name,state,pop_total,income,images'
            }
          }).then((response) => {
            const updateList = this.state.cities.slice()
            updateList.push(response.data.data[0])
            this.setState({cities: updateList})
          })
        )
        this.state.major_data.universities.forEach((element) => {
          axios.get(universitiesUrl, {
            params: {
              name: element.school_name,
              columns: 'school_name,admission_rate,undergrad_size,images'
            }
          }).then((response) => {
            const updateList = this.state.universities.slice()
            updateList.push(response.data)
            this.setState({universities: updateList})
          })
      });
      this.setState({name: majorName, loaded: true})
    })
  }).catch((error) =>
      console.log(error)
    )
    document.title = `${majorName} | My Future Education`
  }

  render() {
    const { classes } = this.props;
    if (this.state.loaded) {
    return(
      <div className={classes.root}>
        <Typography variant="h3" align="center" gutterBottom>
          {this.state.name}
        </Typography>
        <div style={{width: "100%", paddingBottom: "56.25%", position: "relative", marginTop: "2%", marginBottom: "2%"}}>
          <img
            className="Carousel-content"
            src={this.state.major_data.images[0]}
            alt="Subjects"
          />
        </div>
        <div className="Charts">
        <Typography style={{marginTop: '2%', marginBottom: '2%'}} variant="h5">
          Description
        </Typography>
        <Typography style={{marginBottom: '2%'}} variant="subtitle1">
          {this.state.major_data.description[0]}
          <br/>
          <a href={this.state.major_data.wiki_link} target='_blank' rel='noopener noreferrer'>Read more...</a>
        </Typography>
        <Typography style={{marginTop: '2%'}} variant="h6">
          Field of study: {this.state.major_data.area_of_study.replace('_', ' ').replace(/\w\S*/g, function(txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          })}
        </Typography>
        <Typography style={{marginTop: '2%', marginBottom: '-2%'}} variant="h6">
          Starting salary: ${this.state.major_data.starting_salary.toLocaleString(undefined, {maximumFractionDigits: 0})}
        </Typography>
        </div>
        <Divider/>
        <Typography style={{marginTop: '2%'}} variant="h5">
          Learn more about {this.state.name}
        </Typography>
        <div style={{width: "100%", paddingBottom: "56.25%", position: "relative", marginTop: "2%", marginBottom: "4%"}}>
        <iframe 
        width="100%"
        height="100%"
        style={{position: "absolute", top: "0", bottom: "0", left: "0", right: "0"}}
        src={this.state.major_data.youtube_link} 
        frameborder="0" 
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" 
        allowfullscreen></iframe>
        </div>
        <Divider/>
        <Typography style={{marginTop: '2%', marginBottom: '2%'}} variant="h5">
          Top cities for {this.state.name}
        </Typography>
        {this.state.cities.map((city) => 
          <CardActionArea href={`/cities/${city.name}_${city.state}`}>
            <Card className="Card">
            <CardMedia
              image={city.images[0]}
              className="Card-Media"
            />
            <div className="Card-Details">
              <CardContent>
                <Typography variant="h5">
                  {city.name}, {city.state}
                </Typography>
                <Typography variant="subtitle1">
                  Population: {city.pop_total.toLocaleString(undefined, {maximumFractionDigits: 0})}
                </Typography>
                <Typography variant="subtitle1">
                  Median income: ${city.income.toLocaleString(undefined, {minimumFractionDigits: 2})}
                </Typography>
              </CardContent>
            </div>
          </Card>
        </CardActionArea>
        )}
        <Divider/>
        <Typography style={{marginTop: '2%', marginBottom: '2%'}} variant="h5">
          Top Universities for {this.state.name}
        </Typography>
        {this.state.universities.map((university) =>
          <CardActionArea href={`/universities/${university.school_name}`}> 
            <Card className="Card">
              <CardMedia
                image={university.images[0]}
                className="Card-Media"
              />
              <div className="Card-Details">
                <CardContent>
                  <Typography variant="h5">
                    {university.school_name}
                  </Typography>
                  <Typography variant="subtitle1">
                    Admission rate: {university.admission_rate !== null ? (university.admission_rate * 100).toLocaleString(undefined, {maximumFractionDigits: 2}) + "%" : "Not available"}
                  </Typography>
                  <Typography variant="subtitle1">
                    Undergraduate student body size: {university.undergrad_size.toLocaleString(undefined, {minimumFractionDigits: 0})}
                  </Typography>
                </CardContent>
              </div>
            </Card>
          </CardActionArea>
        )}
      </div>
    );
    }
    else {
      return null
    }
  }
}

MajorsInstance.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(MajorsInstance)
