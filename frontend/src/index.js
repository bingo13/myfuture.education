import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import './index.css';
import All from './Components/All';
import * as serviceWorker from './serviceWorker';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#53c4f7',
      main: '#29b6f6',
      dark: '#1c7fac',
      contrastText: '#fff',
    },
    secondary: {
      light: '#9778ce',
      main: '#7e57c2',
      dark: '#583c87',
      contrastText: '#000',
    },
    background: {
      default: '#F5F5F5',
    },
  },
});

ReactDOM.render((
  <MuiThemeProvider theme={theme}>
    <CssBaseline/>
    <BrowserRouter>
      <All />
    </BrowserRouter>
  </MuiThemeProvider>
  ), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
