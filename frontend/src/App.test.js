import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import getTopUniversity from './Pages/Majors/Majors.js'
import Majors from './Pages/Majors/Majors.js';
import { withStyles } from '@material-ui/core';

describe('Top University for Computer Science', () => {
  it('works', () => {
    let v = new Majors();
    expect(v.getTopUniversity({"universities": [
      { "school_name": "California Institute of Technology", "percentage_of_degrees": 0.2249 },
      { "school_name": "Massachusetts Institute of Technology", "percentage_of_degrees": 0.2655 },
      { "school_name": "Rensselaer Polytechnic Institute", "percentage_of_degrees": 0.1192 },
      ]})).toEqual("Massachusetts Institute of Technology")
  })
})

describe('Top University for Biology', () => {
  it('works', () => {
    let v = new Majors();
    expect(v.getTopUniversity({"universities": [
      { "school_name": "Duke University", "percentage_of_degrees": 0.1796},
      { "school_name": "Emory University", "percentage_of_degrees": 0.1904},
      { "school_name": "University of California-Los Angeles", "percentage_of_degrees": 0.1548}
      ]})).toEqual("Emory University")
  })
})

describe('Top University for Marketing', () => {
  it('works', () => {
    let v = new Majors();
    expect(v.getTopUniversity({"universities": [
      { "school_name": "Fashion Institute of Technology", "percentage_of_degrees": 0.4116},
      { "school_name": "Pace University-New York", "percentage_of_degrees": 0.3081},
      { "school_name": "University of San Diego", "percentage_of_degrees": 0.403}
      ]})).toEqual("Fashion Institute of Technology")
  })
})

describe('Top University for History', () => {
  it('works', () => {
    let v = new Majors();
    expect(v.getTopUniversity({"universities": [
      { "school_name": "Harvard University", "percentage_of_degrees": 0.0726},
      { "school_name": "Yale University", "percentage_of_degrees": 0.0746}
      ]})).toEqual("Yale University")
  })
})

describe('Top University for Kinesiology', () => {
  it('works', () => {
    let v = new Majors();
    expect(v.getTopUniversity({"universities": [
      {"school_name": "California Institute of Technology", "percentage_of_degrees": 0.1767},
      {"school_name": "Massachusetts Institute of Technology", "percentage_of_degrees": 0.0774},
      {"school_name": "University of Chicago", "percentage_of_degrees": 0.0853}
      ]})).toEqual("California Institute of Technology")
  })
})

describe('Top City for Kinesiology', () => {
  it('works', () => {
    let v = new Majors();
    expect(v.getTopCity({"cities": [
        {
        "location": "Chicago",
        "major_population": 509.9234
        },
        {
          "location": "Cambridge",
          "major_population": 347.4486
        },
        {
          "location": "Atlanta",
          "major_population": 267.26460000000003
        },
        {
          "location": "Pasadena",
          "major_population": 172.9893
        }
      ]
      })).toEqual("Chicago")
  })
})

describe('Top City for Computer  Science', () => {
  it('works', () => {
    let v = new Majors();
    expect(v.getTopCity({"cities": [
      {
        "location": "Ann Arbor",
        "major_population": 2220.3492
      },
        {
          "location": "Cambridge",
          "major_population": 1617.7979
        },
        {
          "location": "Atlanta",
          "major_population": 1577.0088
        },
        {
          "location": "Richardson",
          "major_population": 1563.9778000000001
        },
        {
          "location": "Berkeley",
          "major_population": 1494.759
        },
        {
          "location": "Troy",
          "major_population": 739.04
        },
        {
          "location": "New York",
          "major_population": 504.5004
        },
        {
          "location": "San Francisco",
          "major_population": 420.5548
        },
        {
          "location": "Pasadena",
          "major_population": 244.269
        }
      ]
      })).toEqual("Ann Arbor")
  })
})


describe('Top City for Kinesiology', () => {
  it('works', () => {
    let v = new Majors();
    expect(v.getTopCity({"cities": [
      {
        "location": "Cambridge",
        "major_population": 540.6522
      },
        {
        "location": "New Haven",
        "major_population": 408.1366
        }
      ]
      })).toEqual("Cambridge")
  })
})

describe('Top City for Biology', () => {
  it('works', () => {
    let v = new Majors();
    expect(v.getTopCity({"cities": [
      {
        "location": "Los Angeles",
        "major_population": 4776.973199999999
      },
        {
          "location": "Madison",
        "major_population": 4208.879999999999
        },
        {
          "location": "College Station",
        "major_population": 3912.2816000000003
        }
      ]
      })).toEqual("Los Angeles")
  })
})

describe('Top City for Marketing', () => {
  it('works', () => {
    let v = new Majors();
    expect(v.getTopCity({"cities": [
      {
        "location": "Houston",
        "major_population": 9832.32
      },
        {
          "location": "New York",
          "major_population": 9231.564
        },
        {
          "location": "Tuscaloosa",
          "major_population": 9115.7777
        },
      ]
      })).toEqual("Houston")
  })
})