import requests
import json
from pprint import pprint

keys = [
    "pop_total",
    "pop_lt_18",
    "pop_18_24",
    "pop_25_34",
    "pop_35_44",
    "pop_45_54",
    "pop_55_64",
    "pop_gt_65",
    "pop_hispanic",
    "pop_total",
    "pop_white",
    "pop_black",
    "pop_native",
    "pop_asian",
    "pop_hawaiian",
    "pop_other",
    "pop_gt_2",
]

abbrvs = {
    "Alabama": "AL",
    "Alaska": "AK",
    "Arizona": "AZ",
    "Arkansas": "AR",
    "California": "CA",
    "Colorado": "CO",
    "Connecticut": "CT",
    "Delaware": "DE",
    "Florida": "FL",
    "Georgia": "GA",
    "Hawaii": "HI",
    "Idaho": "ID",
    "Illinois": "IL",
    "Indiana": "IN",
    "Iowa": "IA",
    "Kansas": "KS",
    "Kentucky": "KY",
    "Louisiana": "LA",
    "Maine": "ME",
    "Maryland": "MD",
    "Massachusetts": "MA",
    "Michigan": "MI",
    "Minnesota": "MN",
    "Mississippi": "MS",
    "Missouri": "MO",
    "Montana": "MT",
    "Nebraska": "NE",
    "Nevada": "NV",
    "New_Hampshire": "NH",
    "New_Jersey": "NJ",
    "New_Mexico": "NM",
    "New_York": "NY",
    "North_Carolina": "NC",
    "North_Dakota": "ND",
    "Ohio": "OH",
    "Oklahoma": "OK",
    "Oregon": "OR",
    "Pennsylvania": "PA",
    "Rhode_Island": "RI",
    "South_Carolina": "SC",
    "South_Dakota": "SD",
    "Tennessee": "TN",
    "Texas": "TX",
    "Utah": "UT",
    "Vermont": "VT",
    "Virginia": "VA",
    "Washington": "WA",
    "West_Virginia": "WV",
    "Wisconsin": "WI",
    "Wyoming": "WY",
    "District_of_Columbia": "DC",
}


def data_usa_calls(state, place, code):
    url_base = "http://api.datausa.io/api/?show=geo&sumlevel=place&year=latest"
    call_1_params = "&required=age,income,median_property_value&geo=16000US"
    call_2_params = "&required=income_black,income_native,income_white,income_asian,income_hispanic,income_hawaiian,income_2ormore&geo=16000US"

    result = requests.get(url_base + call_1_params + code).json()
    data = result["data"][0][2:]
    headers = result["headers"][2:]

    result = requests.get(url_base + call_2_params + code).json()
    data.extend(result["data"][0][2:])
    headers.extend(result["headers"][2:])

    my_dict = {}
    for header, info in zip(headers, data):
        if info is None:
            info = 0
        my_dict[header] = info

    # with open('./output.txt', 'w') as f:
    #     pprint(my_dict, f)
    return my_dict


def census_calls(state, place, state_code, place_code):
    url_base = "https://api.census.gov/data/2016/acs/acsse?key="
    key = "d37aaa3ff544e8e50825ff778fbba83a87a239be"
    call_1_params = "&get=group(K200104)&for=place:"
    call_2_params = "&get=group(K200201),K200301_003E&for=place:"
    location = place_code + "&in=state:" + state_code
    result = requests.get(url_base + key + call_1_params + location).json()
    filtered = [t[1] for t in zip(result[0], result[1]) if t[0].endswith("E")]
    result = requests.get(url_base + key + call_2_params + location).json()
    filtered.extend([t[1] for t in zip(result[0], result[1]) if t[0].endswith("E")])

    my_dict = {}
    for key, info in zip(keys, filtered):
        if type(info) is str and info.isdigit():
            info = int(info)
        my_dict[key] = info
    # with open('./output.txt', 'w') as f:
    #     pprint(my_dict, f)
    return my_dict


def main():
    print("scrapping cities")
    with open("./codes.json") as f:
        data = json.load(f)
    states = data["data"]
    json_list = []
    for state in states:
        state_name = state["state"]
        state_code = state["code"]
        places = state["places"]
        for place in places:
            place_name = place["name"]
            place_code = place["code"]
            my_dict = {
                "name": place_name,
                "state": state_name.replace("_", " "),
                "abbrv": abbrvs[state_name],
            }
            my_dict.update(
                data_usa_calls(state_name, place_name, state_code + place_code)
            )
            my_dict.update(census_calls(state_name, place_name, state_code, place_code))
            json_list.append(my_dict)
            # Todo: input my_dict into database
        #     with open('./output.txt', 'w') as f:
        #         pprint(my_dict, f)
        #     break
        # break
    json_dict = {"data": json_list}
    with open("./cities.json", "w") as f:
        json.dump(json_dict, f)


if __name__ == "__main__":
    main()
