import requests
import re
import json

states = {
    "Alabama": "01",
    "Alaska": "02",
    "Arizona": "04",
    "Arkansas": "05",
    "California": "06",
    "Colorado": "08",
    "Connecticut": "09",
    "Delaware": "10",
    "District_of_Columbia": "11",
    "Florida": "12",
    "Georgia": "13",
    "Hawaii": "15",
    "Idaho": "16",
    "Illinois": "17",
    "Indiana": "18",
    "Iowa": "19",
    "Kansas": "20",
    "Kentucky": "21",
    "Louisiana": "22",
    "Maine": "23",
    "Maryland": "24",
    "Massachusetts": "25",
    "Michigan": "26",
    "Minnesota": "27",
    "Mississippi": "28",
    "Missouri": "29",
    "Montana": "30",
    "Nebraska": "31",
    "Nevada": "32",
    "New_Hampshire": "33",
    "New_Jersey": "34",
    "New_Mexico": "35",
    "New_York": "36",
    "North_Carolina": "37",
    "North_Dakota": "38",
    "Ohio": "39",
    "Oklahoma": "40",
    "Oregon": "41",
    "Pennsylvania": "42",
    "Rhode_Island": "44",
    "South_Carolina": "45",
    "South_Dakota": "46",
    "Tennessee": "47",
    "Texas": "48",
    "Utah": "49",
    "Vermont": "50",
    "Virginia": "51",
    "Washington": "53",
    "West_Virginia": "54",
    "Wisconsin": "55",
    "Wyoming": "56",
}


def get_place_codes(url, list):
    result = requests.get(url)
    data = result.json()[1:]
    cities = []
    for location in data:
        city_dict = {}
        city_dict["name"] = re.sub(
            " city.*| municipality.*| CDP.*| village.*| town.*", "", location[0]
        )
        if city_dict["name"] in list:
            city_dict["code"] = location[2]
            cities.append(city_dict)

    return cities


def main():
    print("scrapping fips")
    url_base = "https://api.census.gov/data/2016/acs/acsse?key="
    key = "d37aaa3ff544e8e50825ff778fbba83a87a239be"
    params = "&get=NAME&for=place:*&in=state:"

    with open("./../university_scraper/university_cities.json") as university_cities:
        university_cities_dict = json.load(university_cities)

    states_info = []
    for state in university_cities_dict.keys():
        code = states[state]
        cities = get_place_codes(
            url_base + key + params + code, university_cities_dict[state]
        )
        # create dict with state: name, places:list, code: code
        d = {"state": state, "code": code, "places": cities}
        states_info.append(d)
        # break

    data = {"data": states_info}
    # f = open('./output1.txt', 'w')
    # f.write(str(data))
    # f.close

    with open("./codes.json", "w") as f:
        json.dump(data, f)


if __name__ == "__main__":
    main()
