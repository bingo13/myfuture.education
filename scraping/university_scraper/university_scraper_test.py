import university_scraper
from unittest import TestCase, main
import os
import json


class TestUniversityScraper(TestCase):
    # used to test locally
    # def test_get_university_info(self):
    #     test_filename = "test_university_ids.txt"
    #
    #     ut_austin_info = {"id": 228778, "name": "The University of Texas at Austin", "alias": "UT Austin", "city": "Austin", "abbrv_state": "TX", "state": "Texas", "school_url": "www.utexas.edu", "price_calculator_url": "www.collegeportraits.org/TX/UTAustin/", "latitude": 30.282825, "longitude": -97.738273, "rate_suppressed_overall": 0.8044, "median_income": 58200, "academics": {"education": 0.0, "mathematics": 0.0235, "business_marketing": 0.2263, "communications_technology": 0.0024, "language": 0.014, "visual_performing": 0.0246, "engineering_technology": 0.0, "parks_recreation_fitness": 0.0184, "agriculture": 0.0, "security_law_enforcement": 0.0008, "computer": 0.0433, "precision_production": 0.0, "humanities": 0.013, "library": 0.0, "psychology": 0.0281, "social_science": 0.0856, "legal": 0.0, "english": 0.0232, "construction": 0.0, "military": 0.0, "communication": 0.091, "public_administration_social_service": 0.0074, "architecture": 0.0034, "ethnic_cultural_gender": 0.0076, "resources": 0.0006, "health": 0.069, "engineering": 0.0968, "history": 0.0142, "theology_religious_vocation": 0.0, "transportation": 0.0, "physical_science": 0.0255, "science_technology": 0.0, "biological": 0.0883, "family_consumer_science": 0.0423, "philosophy_religious": 0.0062, "personal_culinary": 0.0003, "multidiscipline": 0.0443, "mechanic_repair_technology": 0.0}, "size": 39676, "race_ethnicity.white": 0.4237, "race_ethnicity.black": 0.0419, "race_ethnicity.hispanic": 0.2268, "race_ethnicity.asian": 0.2082, "race_ethnicity.aian": 0.0014, "race_ethnicity.nhpi": 0.0015, "race_ethnicity.two_or_more": 0.036, "race_ethnicity.non_resident_alien": 0.0515, "race_ethnicity.unknown": 0.009, "admission_rate": 0.4037, "sat_average": 1289.0, "act_midpoint": 29.0, "in_state": 10092, "out_of_state": 35682, "private": None, "public": 18422}
    #
    #     f = open(test_filename, "w+")
    #     f.write("228778\n243744")
    #     f.close()
    #
    #     universities_info = university_scraper.get_university_info(test_filename)
    #
    #     self.assertEqual(len(universities_info["data"]), 2)
    #     self.assertEqual(universities_info["data"][0], ut_austin_info)
    #
    #     os.remove(test_filename)

    def test_get_university_cities(self):
        json_data = [
            {"school_name": "UT Austin", "city": "Austin", "state": "Texas"},
            {"school_name": "UT Dallas", "city": "Dallas", "state": "Texas"},
            {"school_name": "Stanford", "city": "Stanford City", "state": "California"},
        ]
        expected_value = {"Texas": ["Austin", "Dallas"], "California": ["Stanford"]}
        self.assertEqual(
            university_scraper.get_university_cities(json_data), expected_value
        )

    def test_abbreviations_to_states(self):
        self.assertEqual(university_scraper.abbreviations_to_states("TX"), "Texas")

    def test_all_universities_scraped(self):
        if os.path.isfile("university_info.json"):
            with open("university_info.json") as f:
                university_info_json = json.load(f)
                self.assertEqual(len(university_info_json["data"]), 69)


if __name__ == "__main__":
    main()
