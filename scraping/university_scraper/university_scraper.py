import requests
import json
import os


def main():
    universities_info = get_university_info("university_ids.txt")
    with open("university_info.json", "w+") as f:
        json.dump(universities_info, f)

    universities_cities = get_university_cities(universities_info["data"])
    with open("university_cities.json", "w+") as f:
        json.dump(universities_cities, f)


def get_university_info(university_ids_filename):
    universities_info = []

    api_key = os.environ.get("COLLEGE_SCORECARD_API_KEY")
    csc_url = (
        "https://api.data.gov/ed/collegescorecard/v1/schools?api_key=" + api_key + "&"
    )

    with open(university_ids_filename) as f:
        for line in f:
            university_id = line.strip()
            parameters = {"id": university_id}

            response = requests.get(csc_url, parameters)
            json_response = response.json()
            results = json_response["results"][0]

            university = dict()

            university["id"] = results["id"]
            assert str(university["id"]) == university_id

            school_info = results["school"]
            university["name"] = school_info["name"]
            university["alias"] = school_info["alias"]
            university["city"] = school_info["city"]
            university["abbrv_state"] = school_info["state"]
            university["state"] = abbreviations_to_states(university["abbrv_state"])
            university["school_url"] = school_info["school_url"]
            university["price_calculator_url"] = school_info["price_calculator_url"]

            location = results["location"]
            university["latitude"] = location["lat"]
            university["longitude"] = location["lon"]

            latest = results["latest"]
            university["rate_suppressed_overall"] = latest["completion"][
                "rate_suppressed"
            ]["overall"]
            university["median_income"] = latest["earnings"]["10_yrs_after_entry"][
                "median"
            ]
            university["academics"] = latest["academics"]["program_percentage"]

            student = latest["student"]
            university["size"] = student["size"]
            demographics = student["demographics"]
            race_ethnicity = demographics["race_ethnicity"]
            university["race_ethnicity.white"] = race_ethnicity["white"]
            university["race_ethnicity.black"] = race_ethnicity["black"]
            university["race_ethnicity.hispanic"] = race_ethnicity["hispanic"]
            university["race_ethnicity.asian"] = race_ethnicity["asian"]
            university["race_ethnicity.aian"] = race_ethnicity["aian"]
            university["race_ethnicity.nhpi"] = race_ethnicity["nhpi"]
            university["race_ethnicity.two_or_more"] = race_ethnicity["two_or_more"]
            university["race_ethnicity.non_resident_alien"] = race_ethnicity[
                "non_resident_alien"
            ]
            university["race_ethnicity.unknown"] = race_ethnicity["unknown"]

            admissions = latest["admissions"]
            university["admission_rate"] = admissions["admission_rate"]["overall"]
            university["sat_average"] = admissions["sat_scores"]["average"]["overall"]
            university["act_midpoint"] = admissions["act_scores"]["midpoint"][
                "cumulative"
            ]

            cost = latest["cost"]
            tuition = cost["tuition"]
            university["in_state"] = tuition["in_state"]
            university["out_of_state"] = tuition["out_of_state"]
            avg_net_price = cost["avg_net_price"]
            university["private"] = avg_net_price["private"]
            university["public"] = avg_net_price["public"]

            universities_info.append(university)

    json_formatted_info = dict()
    json_formatted_info["data"] = universities_info
    return json_formatted_info


def get_university_cities(universities_info):
    university_cities = {}

    for university in universities_info:
        city = university["city"].replace(" City", "")
        state = university["state"]

        if state in university_cities:
            university_cities[state].append(city)
        else:
            university_cities[state] = [city]

    return university_cities


def abbreviations_to_states(abbreviation):
    states = {
        "AL": "Alabama",
        "AK": "Alaska",
        "AZ": "Arizona",
        "AR": "Arkansas",
        "CA": "California",
        "CO": "Colorado",
        "CT": "Connecticut",
        "DE": "Delaware",
        "FL": "Florida",
        "GA": "Georgia",
        "HI": "Hawaii",
        "ID": "Idaho",
        "IL": "Illinois",
        "IN": "Indiana",
        "IA": "Iowa",
        "KS": "Kansas",
        "KY": "Kentucky",
        "LA": "Louisiana",
        "ME": "Maine",
        "MD": "Maryland",
        "MA": "Massachusetts",
        "MI": "Michigan",
        "MN": "Minnesota",
        "MS": "Mississippi",
        "MO": "Missouri",
        "MT": "Montana",
        "NE": "Nebraska",
        "NV": "Nevada",
        "NH": "New Hampshire",
        "NJ": "New Jersey",
        "NM": "New Mexico",
        "NY": "New York",
        "NC": "North Carolina",
        "ND": "North Dakota",
        "OH": "Ohio",
        "OK": "Oklahoma",
        "OR": "Oregon",
        "PA": "Pennsylvania",
        "RI": "Rhode Island",
        "SC": "South Carolina",
        "SD": "South Dakota",
        "TN": "Tennessee",
        "TX": "Texas",
        "UT": "Utah",
        "VT": "Vermont",
        "VA": "Virginia",
        "WA": "Washington",
        "WV": "West Virginia",
        "WI": "Wisconsin",
        "WY": "Wyoming",
        "DC": "District of Columbia",
    }

    return states[abbreviation]


if __name__ == "__main__":
    main()
