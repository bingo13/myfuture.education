import requests
import re
import os
import linecache
import json


def main():
    majors_info = get_majors_info("majors.txt")
    with open("majors_info.json", "w+") as f:
        json.dump(majors_info, f)


def get_majors_info(text_file):
    majors_info = {"data": []}

    wiki_url = "https://en.wikipedia.org/w/api.php?"

    with open(text_file) as mf:
        for line in mf:
            major = line.strip()
            parameters = {"action": "parse", "format": "json", "page": major}

            response = requests.get(wiki_url, parameters)
            json_response = response.json()
            raw_text = json_response["parse"]["text"]["*"]
            cleaned_text = clean_text(raw_text)

            wf = open(major + "_clean.txt", "w+", encoding="utf-8")
            wf.write(cleaned_text)
            wf.close()

            majors_info["data"].append(get_main_description(major, cleaned_text))

    return majors_info


def get_main_description(major, cleaned_text):
    # only get the part starting with the description of the wiki page
    filename = major + "_clean.txt"  # cleaned_text is in this file also
    resulting_text = ""
    parsed_filename = ""

    use_vte_parsing = False

    # handles oddities in different wiki pages
    if major == "architecture":
        resulting_text = linecache.getline(filename, 21)
    elif major == "history":
        for i in range(19, 26):
            resulting_text += linecache.getline(filename, i)
    elif major == "kinesiology":
        for i in range(5, 8):
            resulting_text += linecache.getline(filename, i)
    elif major == "mathematics":
        for i in range(15, 22):
            resulting_text += linecache.getline(filename, i)
    elif major == "mechanical engineering":
        for i in range(9, 14):
            resulting_text += linecache.getline(filename, i)
    elif major == "social work":
        for i in range(7, 10):
            resulting_text += linecache.getline(filename, i)
    elif major == "spanish language":
        for i in range(129, 146):
            resulting_text += linecache.getline(filename, i)
    else:
        # handles normal case
        # proceeding "vte", description begins
        index_of_vte = cleaned_text.find("vte")
        resulting_text = cleaned_text[index_of_vte:]

        use_vte_parsing = True

        wf = open(major + ".txt", "w+", encoding="utf-8")
        wf.write(resulting_text)
        wf.close()

        parsed_filename = get_truncated_normal_case_descriptions(major)

    if not use_vte_parsing:
        parsed_filename = major + "_parsed.txt"
        parsed_wf = open(parsed_filename, "w+", encoding="utf-8")
        parsed_wf.write(resulting_text)
        parsed_wf.close()

    if os.path.exists(major + ".txt"):
        os.remove(major + ".txt")
    if os.path.exists(major + "_clean.txt"):
        os.remove(major + "_clean.txt")

    return parsed_filename


def get_truncated_normal_case_descriptions(major):
    parsed_filename = major + "_parsed.txt"

    with open(major + ".txt", encoding="utf-8") as wf:
        next(wf)
        next(wf)

        parsed_wf = open(parsed_filename, "w+", encoding="utf-8")

        # truncate end so that only relevant description is included
        blank_count = 0
        for wf_line in wf:
            if wf_line.replace(" ", "") == "\n":
                blank_count += 1
            else:
                blank_count = 0
            if blank_count == 2:
                break

            parsed_wf.write(wf_line.replace(";pageneeded;", ""))

        parsed_wf.close()

    return parsed_filename


def clean_text(raw_text):
    clean_comp = re.compile("<.*?>")
    cleaned_text = re.sub(clean_comp, "", raw_text)
    cleaned_text = cleaned_text.replace("\n", " \n")
    cleaned_text = re.sub("&#\d\d", "", cleaned_text)
    cleaned_text = re.sub(";\d\d\d;", "", cleaned_text)
    cleaned_text = re.sub("0;", "", cleaned_text)
    cleaned_text = re.sub("•0;", "", cleaned_text)
    cleaned_text = os.linesep.join([s for s in cleaned_text.splitlines() if s])
    cleaned_text = re.sub('"', "", cleaned_text)
    cleaned_text = re.sub("'", "", cleaned_text)
    cleaned_text = re.sub(";\d;", "", cleaned_text)
    cleaned_text = re.sub(";\d\d;", "", cleaned_text)
    cleaned_text = re.sub(";\d", "", cleaned_text)
    return cleaned_text


if __name__ == "__main__":
    main()
