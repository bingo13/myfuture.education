import wiki_scraper
from unittest import TestCase, main
import os
import json


class TestWikiScraper(TestCase):
    def test_get_majors_info(self):
        test_filename = "test_get_majors_info.txt"

        f = open(test_filename, "w+", encoding="utf-8")
        f.write("spanish language")
        f.close()

        majors_info = wiki_scraper.get_majors_info(test_filename)

        self.assertEqual(majors_info["data"], ["spanish language_parsed.txt"])

        count = 0
        with open("spanish language_parsed.txt", encoding="utf-8") as f:
            for line in f:
                count += 1
        self.assertEqual(count, 17)

        os.remove(test_filename)
        os.remove("spanish language_parsed.txt")

    def test_get_main_description(self):
        major = "test major"
        test_filename = major + ".txt"
        cleaned_text = "blahvte\n\nblah;pageneeded; blah\nblah\n\n \nblah blah blah"

        f = open(test_filename, "w+", encoding="utf-8")
        f.write(cleaned_text)
        f.close()

        parsed_filename = wiki_scraper.get_main_description(major, cleaned_text)

        self.assertEqual(parsed_filename, major + "_parsed.txt")

        actual_result = ""
        with open(parsed_filename, encoding="utf-8") as f:
            for line in f:
                actual_result += line

        self.assertEqual(actual_result, "blah blah\nblah\n\n")

        os.remove(major + "_parsed.txt")

    def test_get_truncated_normal_case_descriptions(self):
        major = "test major"
        test_filename = major + ".txt"
        cleaned_text = "blahvte\n\nblah;pageneeded; blah\nblah\n\n \nblah blah blah"

        f = open(test_filename, "w+", encoding="utf-8")
        f.write(cleaned_text)
        f.close()

        parsed_filename = wiki_scraper.get_truncated_normal_case_descriptions(major)

        self.assertEqual(parsed_filename, major + "_parsed.txt")

        actual_result = ""
        with open(parsed_filename, encoding="utf-8") as f:
            for line in f:
                actual_result += line

        self.assertEqual(actual_result, "blah blah\nblah\n\n")

        os.remove(major + ".txt")
        os.remove(major + "_parsed.txt")

    def test_clean_text(self):
        raw_text = '<p><b>Sociology</b> is the <a href="/wiki/Science" title="Science">scientific study</a> of <a href="/wiki/Society" title="Society">society</a>, patterns of social relationships,'
        expected_clean_text = "Sociology is the scientific study of society, patterns of social relationships,"

        actual_clean_text = wiki_scraper.clean_text(raw_text)

        self.assertEqual(actual_clean_text, expected_clean_text)

    # run this test when scraping is re-done locally (do not do it in GitLab CI)
    # def test_wiki_scraper(self):
    #     wiki_scraper.main()
    #
    #     with open("majors_info.json", encoding="utf-8") as f:
    #         actual_data = json.load(f)["data"]
    #
    #     with open("correctly_parsed_majors/majors_info.json", encoding="utf-8") as correct_f:
    #         expected_data = json.load(correct_f)["data"]
    #
    #     self.assertEqual(actual_data, expected_data)
    #
    #     for major_file in actual_data:
    #         with open(major_file, encoding="utf-8") as actual_file:
    #             with open("correctly_parsed_majors/" + major_file, encoding="utf-8") as expected_file:
    #                 for actual_line, expected_line in zip(actual_file, expected_file):
    #                     self.assertEqual(actual_line, expected_line)


if __name__ == "__main__":
    main()
